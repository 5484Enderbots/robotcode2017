package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.util.EdgeDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by guinea on 1/4/18.
 */

public class ServoTester extends OpMode {
    class ServoTest {
        public String name;
        public Servo svo;
        public double currentPos = 0;
        public double pos1 = 0;
        public double pos2 = 0;

        public ServoTest(String servoName, Servo servo) {
            name = servoName;
            svo = servo;
        }
    }
    List<ServoTest> servos;
    int lastPos = 0;
    int pos = 0;
    @Override
    public void init() {
        servos = new ArrayList<>();
        for (Map.Entry<String, Servo> entry : hardwareMap.servo.entrySet()) {
            servos.add(new ServoTest(entry.getKey(), entry.getValue()));
        }
    }

    @Override
    public void loop() {
        if (servos.size() == 0) {
            telemetry.addLine("No servos detected!");
            return;
        }

        ServoTest servo = servos.get(pos);
        if (pos != lastPos)
            servo.svo.setPosition(servo.currentPos);
        servo.currentPos = servo.svo.getPosition();

        telemetry.addData("Servo name: ", servo.name);
        telemetry.addData("Servo position: ", servo.currentPos);
        telemetry.addData("Servo pos1: ", servo.pos1);
        telemetry.addData("Servo pos2: ", servo.pos2);

    }

    @Override
    public void stop() {
    }
}
