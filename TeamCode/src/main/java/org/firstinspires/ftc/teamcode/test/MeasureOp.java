package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;
import org.firstinspires.ftc.teamcode.util.DriveBase;

/**
 * Created by Avery on 9/11/17.
 */
@Disabled
@TeleOp(name="MeasureOp", group="K9bot")
public class MeasureOp extends OpMode {

    TouchSensor LowLimmit;
    TouchSensor HighLimmit;
    public boolean LowStop = false;
    public boolean HighStop = false;
    public boolean DispenserMoving = false;

   // ModernRoboticsI2cColorSensor snsColorJewel;
    ColorSensor16Bit snsColorJewel;


    Servo svoStone;
    Servo svoJewel;
    DriveBase driveBase;
    DcMotor mtrCollector;
    Servo svoGlyph1;
    Servo svoGlyph2;
    Servo svoCollect;
    Servo orientationSVO;
    //Servo dispensingSVO;
    DcMotor mtrDispense;
    DcMotor mtrExtend;
    //DcMotor mtrAngle;
    int rotLeft = 1850;
    int rotUp = 0;
    int rotDown = 3000;
    double rotPower = 0.6;

    public static final double MEDIUM_POWER = 3;
    public static final double LOW_POWER = 6;
    boolean xButton, xButtonPressed, xLatchState;

    public boolean Run_Collector = false;
    public boolean Stop_Collector = true;
    public boolean Slow_Collector = true;
    public boolean Fast_Collector = false;
    public boolean Reverse_Collector = false;

    boolean dispenserRaising = false;

    public double CollectorPower;
    public double anglePower = 0.4;
    public double extendPower = 0.5;

    ElapsedTime dispenserRuntime = new ElapsedTime();

    public void init_loop() {
        // pass, if we need it, it can be used later. both init and init_loop get called during opmode init.
    }
    @Override
    public void init(){

        driveBase = new DriveBase(hardwareMap, false);

        //HardwareMaps
        orientationSVO = hardwareMap.servo.get("orientationSVO");

        mtrDispense = hardwareMap.dcMotor.get("mtrDispense");
        mtrCollector = hardwareMap.dcMotor.get("mtrCollector");
        svoGlyph1 = hardwareMap.servo.get("svoGlyph1");
        svoGlyph2 = hardwareMap.servo.get("svoGlyph2");
        svoJewel = hardwareMap.servo.get("svoJewel");
        svoCollect = hardwareMap.servo.get("svoCollect");
        svoStone = hardwareMap.servo.get("svoStone");
        //mtrAngle = hardwareMap.dcMotor.get("mtrAngle");
        mtrExtend = hardwareMap.dcMotor.get("mtrExtend");
        svoStone.setPosition(0);
        svoCollect.setPosition(0.5);
        svoJewel.setPosition(0.7);
        LowLimmit = hardwareMap.touchSensor.get("LowLimmit");
        HighLimmit = hardwareMap.touchSensor.get("HighLimmit");
      //  mtrDispense.setMode(DcMotor.RunMode.RUN_TO_POSITION);
      //  snsColorJewel = (ModernRoboticsI2cColorSensor) hardwareMap.get(ColorSensor.class, "snsColorJ");
      //  snsColorJewel.setI2cAddress(I2cAddr.create8bit(0x38));
      //  snsColorJewel.enableLed(true);
        snsColorJewel = new ColorSensor16Bit(hardwareMap, "snsColorJewel", 0x38);

    }
    //Start is pressed
    public void start() {
        orientationSVO.setPosition(0.1098);
        CollectorPower = -0.3;
        svoStone.scaleRange(0, 1);
    }
    @Override
    public void loop(){



        /**
        ||
        ||  CODE FOR PUSHING OUT THE COLLECTOR & BRINGING UP JEWEL ARM.
        ||
         */
        if (gamepad1.y){
            svoCollect.setPosition(0);
        }
        else {
            svoCollect.setPosition(0.5);
        }

        if (gamepad2.dpad_right) {
            svoJewel.setPosition(0.7);
        }
         /**
        ||
        ||  CODE FOR PUSHING OUT THE COLLECTOR & BRINGING UP JEWEL ARM.
        ||
         */




         /**
        ||
        ||  CODE FOR OPERATING DRIVETRAIN
        ||
         */
        // Factor to divide the powers by
        double powerdiv = (1.7);
        //This controls the drive train
        double x1 = gamepad1.left_stick_x;
        double y1 = -gamepad1.left_stick_y;
        double x2 = gamepad1.right_stick_x;

        if (gamepad1.left_bumper) powerdiv = MEDIUM_POWER;
        if (gamepad1.left_trigger > 0.5) powerdiv = LOW_POWER;

        driveBase.driveArcade(x1, y1, x2, powerdiv);
        /**
         ||
         ||  CODE FOR OPERATING DRIVETRAIN
         ||
         */




        /**
         ||
         ||  CODE FOR MEASURING JEWEL COLOR SENSOR
         ||
         */
        if (gamepad1.a){
            int [] JewelColor = snsColorJewel.ReadColor16();
            int blueJ = JewelColor [2];
            int RedJ = JewelColor [0];
            int ColorCompare = blueJ - RedJ;
            telemetry.addData("Blue Color Reading", blueJ);
            telemetry.addData("Red Color Reading", RedJ);
            telemetry.addData("Blue - Red. + = Blue, - = Red.", ColorCompare);
            telemetry.update();
        }

        /**
         ||
         ||  CODE FOR MEASURING JEWEL COLOR SENSOR
         ||
         */





        /**
         ||
         ||  CODE FOR OPERATING THE DISPENSER
         ||
         */
        if (gamepad2.dpad_up){
            mtrDispense.setPower(0.5);
        }else if (HighLimmit.isPressed() && !gamepad2.dpad_down){
            mtrDispense.setPower(0);}


        if (gamepad2.dpad_down){
            mtrDispense.setPower(-0.5);
        }
        else if (LowLimmit.isPressed() && !gamepad2.dpad_up){
            mtrDispense.setPower(0);
        }


       /*if (gamepad2.dpad_left){
            mtrDispense.setTargetPosition(rotLeft);
            mtrDispense.setPower(rotPower);
        }

        if (gamepad2.dpad_down && !LowStop){
            mtrDispense.setTargetPosition(rotDown);
            mtrDispense.setPower(rotPower);
        }

        if (gamepad2.dpad_up && !HighStop){
            mtrDispense.setTargetPosition(rotUp);
            mtrDispense.setPower(rotPower);
        }

        if (LowStop && !gamepad2.dpad_left && !gamepad2.dpad_up){mtrDispense.setPower(0);}
        if (HighStop && !gamepad2.dpad_left && !gamepad2.dpad_down){mtrDispense.setPower(0);}

        if (HighLimmit.isPressed()){HighStop = true;} else{HighStop = false;}
        if (LowLimmit.isPressed()){LowStop = true;} else{LowStop = false;}*/
        /**
         ||
         ||  CODE FOR OPERATING THE DISPENSER
         ||
         */




        /**
         ||
         ||  CODE FOR OPERATING THE BALANCING STONE AND THE ORIENTATION SERVO
         ||
         */
        if (gamepad1.dpad_down){svoStone.setPosition(0.8);}
        else if (gamepad1.dpad_up){svoStone.setPosition(0.17647);}

        //Single press of A, B, X, and Y on gamepad 2 results in servo orientating angles of 0, 90, 180, and 270
        if (gamepad2.x) {orientationSVO.setPosition(0.90196);}

        if (gamepad2.y) {orientationSVO.setPosition(0.6275);}

        if (gamepad2.b) {orientationSVO.setPosition(0.3529);}

        if (gamepad2.a) {orientationSVO.setPosition(0.1098);}
        /**
         ||
         ||  CODE FOR OPERATING THE BALANCING STONE AND THE ORIENTATION SERVO
         ||
         */







        /**
         ||
         ||  CODE FOR OPERATING GLYPH GRABBERS
         ||
         */
        if (gamepad2.left_trigger > 0.5 && !gamepad2.left_bumper){
            svoGlyph1.setPosition(0);
            svoGlyph2.setPosition(1);
        }
        else if (gamepad2.left_bumper && gamepad2.left_trigger > 0.5){
            svoGlyph1.setPosition(1);
            svoGlyph2.setPosition(0);
        }
        else{
            svoGlyph1.setPosition(0.5);
            svoGlyph2.setPosition(0.8);
        }
        /**
         ||
         ||  CODE FOR OPERATING THE GLYPH GRABBERS
         ||
         */




        /**
         ||
         ||  CODE FOR OPERATING THE RELIC ANGLING, EXTENTION, AND GRABBING
         ||
         */
        if (gamepad2.right_stick_y > 0.5){
            mtrExtend.setPower(extendPower);
        }
        else if (gamepad2.right_stick_y < -0.5){
            mtrExtend.setPower(-extendPower);
        }
        else{
            mtrExtend.setPower(0);
        }

        if (gamepad2.left_stick_y > 0.5){
          //  mtrAngle.setPower(anglePower);
        }
        else if (gamepad2.left_stick_y < -0.5){
          //  mtrAngle.setPower(-anglePower);
        }
        else{
          //  mtrAngle.setPower(0);
        }
        /**
         ||
         ||  CODE FOR OPERATING THE RELIC ANGLING, EXTENTION, AND GRABBING
         ||
         */







        /**
         ||
         ||  CODE FOR OPERATING COLLECTOR
         ||
         */
        if (gamepad1.x){
            if (!xButton){
                xLatchState = !xLatchState;
                xButton = !xButton;
            } else {}}
        else {xButton = false;}


        if (xLatchState) {
            Run_Collector = true;
            Stop_Collector = false;
        }
        else {
            Run_Collector = false;
            Stop_Collector = true;
        }

        if (gamepad1.b){
            Fast_Collector = true;
            Slow_Collector = false;
            Reverse_Collector = false;
        }
        else if (gamepad1.a){
            Fast_Collector = false;
            Slow_Collector = true;
            Reverse_Collector = false;
        }


        if (gamepad1.right_trigger > 0.5){
            mtrCollector.setPower(-0.5);
            Stop_Collector = false;
            Run_Collector = false;
        }
        else {

        }

        if (Fast_Collector){
            CollectorPower = 0.3;
        }
        else if (Slow_Collector){
            CollectorPower = 0.3;
        }
        if (Reverse_Collector){
            CollectorPower = -0.5;
        }


        if (Run_Collector){
            mtrCollector.setPower(CollectorPower);
        }
        else if (Stop_Collector) {
            mtrCollector.setPower(0);
        }
        /**
         ||
         ||  CODE FOR OPERATING COLLECTOR
         ||
         */






    }

    public void stop() {

    }

}
