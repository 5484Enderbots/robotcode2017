package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;


/**
 * Created by bensterbenk on 9/18/17.
 */
@Disabled
@TeleOp(name = "Memeboi")
public class TeleopTest extends LinearOpMode {
    private DcMotor motorFL;
    private DcMotor motorFR;

    private Servo armServo;

    private static final double ARM_RETRACTED_POSITION = 0.2;
    private static final double ARM_EXTENDED_POSITION = 0.8;

    @Override
    public void runOpMode() throws InterruptedException {

        motorFL = hardwareMap.dcMotor.get("mtrFL");
        motorFR = hardwareMap.dcMotor.get("mtrFR");


        motorFL.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        armServo = hardwareMap.servo.get("armServo");

        while (opModeIsActive()) {

            motorFL.setPower(-gamepad1.left_stick_y);
            motorFR.setPower(-gamepad1.right_stick_y);

            if (gamepad2.a) {
                armServo.setPosition(0.9);

            }
            if (gamepad2.a) {
                armServo.setPosition(0.2);
            }
            idle();


        }
    }


}