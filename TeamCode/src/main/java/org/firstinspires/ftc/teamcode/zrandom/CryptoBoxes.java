package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;

/**
 * Created by Avery on 10/2/17.
 */
@Disabled
@Autonomous(name = "CryptoBoxTest")
public class CryptoBoxes extends LinearOpMode{
    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorB;
    ColorSensor16Bit snsColorT;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;
    ModernRoboticsI2cGyro snsGyro;
    Servo svoArm;

    @Override
    public void runOpMode() {
        snsColorF = new ColorSensor16Bit(hardwareMap, "snsColorF", 0x3a);
        snsColorB = new ColorSensor16Bit(hardwareMap, "snsColorB", 0x3c);
        snsColorT = new ColorSensor16Bit(hardwareMap, "snsColorT", 0x3e);
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        svoArm = hardwareMap.servo.get("svoArm");
        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");
        snsGyro.calibrate();
        telemetry.log().add("Do not move, Gyro is calibrating");

       // mtrBL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);


        telemetry.update();
        waitForStart();

        int[] Values16B = snsColorB.ReadColor16();
        int redB = Values16B[0];
        int[] Values16F = snsColorF.ReadColor16();
        int redF = Values16F[0];
        int[] Values16T = snsColorT.ReadColor16();
        int redT = Values16F[0];
        float gyroValue = snsGyro.getIntegratedZValue();
        double value = 0.2;

        while(redF < 10) {
            Values16F = snsColorB.ReadColor16();
            redF = Values16F[0];
            mtrFR.setPower(0.1);
            mtrFL.setPower(-0.1);
            mtrBR.setPower(0.1);
            mtrBL.setPower(-0.1);

        }
        mtrFR.setPower(0);
        mtrFL.setPower(0);
        mtrBR.setPower(0);
        mtrBL.setPower(0);
        sleep(250);
        mtrFR.setPower(-0.1);
        mtrFL.setPower(0.1);
        mtrBR.setPower(-0.1);
        mtrBL.setPower(0.1);
        sleep(400);
        mtrFR.setPower(0);
        mtrFL.setPower(0);
        mtrBR.setPower(0);
        mtrBL.setPower(0);
        sleep(200);
        while(redT < 20) {
            Values16T = snsColorT.ReadColor16();
            redT = Values16T[0];
            mtrFL.setPower(0.2);
            mtrFR.setPower(0.2);
            mtrBL.setPower(-0.2);
            mtrBR.setPower(-0.2);

        }

        mtrFL.setPower(0);
        mtrFR.setPower(0);
        mtrBL.setPower(0);
        mtrBR.setPower(0);
        sleep(1000);
        mtrFL.setPower(0.2);
        mtrFR.setPower(0.2);
        mtrBL.setPower(-0.2);
        mtrBR.setPower(-0.2);
        sleep(500);
        mtrFL.setPower(0);
        mtrFR.setPower(0);
        mtrBL.setPower(0);
        mtrBR.setPower(0);
        sleep(500);
        Values16T = snsColorT.ReadColor16();
        redT = Values16T[0];
        while(redT < 20) {
            Values16T = snsColorT.ReadColor16();
            redT = Values16T[0];
            mtrFL.setPower(0.2);
            mtrFR.setPower(0.2);
            mtrBL.setPower(-0.2);
            mtrBR.setPower(-0.2);

        }
        mtrFL.setPower(-0.2);
        mtrFR.setPower(-0.2);
        mtrBL.setPower(0.2);
        mtrBR.setPower(0.2);
        sleep(500);
        mtrFL.setPower(0);
        mtrFR.setPower(0);
        mtrBL.setPower(0);
        mtrBR.setPower(0);
    }
}
