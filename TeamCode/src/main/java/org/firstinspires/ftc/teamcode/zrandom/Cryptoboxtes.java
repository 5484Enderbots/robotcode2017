package org.firstinspires.ftc.teamcode.zrandom;


import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by bensterbenk on 10/29/17.
 */
@Disabled
@TeleOp(name = "TeleopCryptoTest")
public class Cryptoboxtes extends OpMode{


    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;

    public void init() {

        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
    }
    @Override
    public void loop(){

        double x1 = gamepad1.left_stick_x;
        double y1 = -gamepad1.left_stick_y;
        double x2 = gamepad1.right_stick_x;

        double frontLeft;
        double frontRight;
        double backLeft;
        double backRight;

        frontLeft = x1 - y1 - x2;
        frontRight = x1 + y1 -x2;
        backLeft = -x1 - y1 - x2;
        backRight = -x1 +y1 -x2;

        mtrFL.setPower(Range.clip(frontLeft, -1, 1));
        mtrFR.setPower(Range.clip(frontRight, -1, 1));
        mtrBL.setPower(Range.clip(backLeft, -1, 1));
        mtrBR.setPower(Range.clip(backRight, -1, 1));



    }
}
