package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.GyroSensor;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;

/**
 * Created by bensterbenk on 10/9/17.
 */
@Disabled
@Autonomous(name = "GyroSensorRead")

public class sdaf extends LinearOpMode{

    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorB;
    GyroSensor snsGyro;
    ModernRoboticsI2cGyro snsI2cgyro;
    int xrate = 0;
    int yrate = 0;
    int zrate = 0;
    int heading = 0;
    int zaccumulated = 0;

    @Override
    public void runOpMode() {

        snsColorF = new ColorSensor16Bit(hardwareMap , "snsColorF", 0x3a);
        snsColorB = new ColorSensor16Bit(hardwareMap , "snsColorB", 0x3c);

        snsGyro = hardwareMap.gyroSensor.get("snsGyro");
        snsI2cgyro = (ModernRoboticsI2cGyro) snsGyro;


        snsI2cgyro.calibrate();
        while (snsI2cgyro.isCalibrating()){

        }
        telemetry.addLine(" Ready for start");
        telemetry.update();
        waitForStart();
        int forever = 1;


        while(forever == 1) {

            xrate = snsI2cgyro.rawX();
            yrate = snsI2cgyro.rawY();
            zrate = snsI2cgyro.rawZ();
            heading = snsI2cgyro.getHeading();
            zaccumulated = snsI2cgyro.getIntegratedZValue();

            telemetry.addData("X rate: ", String.format("%d", xrate));
            telemetry.addData("Y rate: ", String.format("%d", yrate));
            telemetry.addData("Z rate: ", String.format("%d", zrate));
            telemetry.addData("heading: ", String.format("%d", heading));
            telemetry.addData("Z accumulated:", String.format("%d", zaccumulated));
            telemetry.addData(" Y Value " + yrate, " Z Value " + zrate, " Heading " +heading, " Z accumulated " +zaccumulated);
            telemetry.update();


        }


    }
}
