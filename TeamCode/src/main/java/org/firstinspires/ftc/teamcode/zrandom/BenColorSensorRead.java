package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;

/**
 * Created by bensterbenk on 10/9/17.
 */
@Disabled
@TeleOp(name = "bencolorsensortest")

public class BenColorSensorRead extends LinearOpMode{

    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorB;
    ColorSensor16Bit snsColorT;

    @Override
    public void runOpMode() {

        snsColorF = new ColorSensor16Bit(hardwareMap , "snsColorF", 0x3a);
        snsColorB = new ColorSensor16Bit(hardwareMap , "snsColorB", 0x3c);
        snsColorT = new ColorSensor16Bit(hardwareMap , "snsColorT", 0x3e);

        telemetry.addLine(" Ready for start");
        telemetry.update();
        waitForStart();
        int forever = 1;
        int[] Values16F = snsColorF.ReadColor16();
        int redF = Values16F[0];
        int[] Values16B = snsColorB.ReadColor16();
        int redB = Values16B[0];
        int[] Values16T = snsColorT.ReadColor16();
        int redT = Values16T[0];

        while(forever == 1) {
            Values16F = snsColorF.ReadColor16();
            redF = Values16F[0];

            Values16B = snsColorB.ReadColor16();
            redB = Values16B[0];

            Values16T = snsColorT.ReadColor16();
            redT = Values16T[0];

            telemetry.addData("moving, Front: ", String.format("%d", Values16F[0]));
            telemetry.addData(" Back: ", String.format("%d", Values16B[0]));
            telemetry.addData("Top: ", String.format("%d", Values16T[0]));
            telemetry.update();

        }

        telemetry.addData("Stopped, color value is: ", String.format("%d", Values16B[0]));
        telemetry.update();

    }
}
