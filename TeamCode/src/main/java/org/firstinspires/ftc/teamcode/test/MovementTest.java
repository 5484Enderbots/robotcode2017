package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.teamcode.util.AutonomousOpMode;

/**
 * Created by guinea on 10/29/17.
 */
@Disabled
@Autonomous(name="Movement test", group="test")
public class MovementTest extends AutonomousOpMode {

    @Override
    public void runInit() throws InterruptedException {

    }

    @Override
    public void runAutonomous() {
        driveDistance(0.5, FORWARD, 2);
        driveDistance(0.5, LEFT, 2);
        driveDistance(0.5, BACKWARD, 2);
        driveDistance(0.5, RIGHT, 2);
        turnAngle(0.05, 90);
    }

}
