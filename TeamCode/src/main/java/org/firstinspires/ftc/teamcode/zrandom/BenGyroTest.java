package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by Ben on 10/22/17.
 */
@Disabled
@TeleOp (name = "BenGyroTest", group = "K9bot")
public class BenGyroTest extends OpMode {


    ModernRoboticsI2cGyro snsGyro;

    public void init() {

        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");

        telemetry.log().add("Gyro Calibrating. Do Not Move!");
        snsGyro.calibrate();

    }
    public void loop(){

        float gyroZValue = snsGyro.getIntegratedZValue();
        float gyroHeadingValue = snsGyro.getHeading();
        float gyroYValue = snsGyro.rawY();
        float gyroXValue = snsGyro.rawX();

        telemetry.addData("Gyro ZValue:", gyroZValue);
        telemetry.addData("Gyro Heading:", gyroHeadingValue);
        telemetry.addData("Gyro YValue:", gyroYValue);
        telemetry.addData("Gyro XValue:", gyroXValue);

        telemetry.update();

    }

}