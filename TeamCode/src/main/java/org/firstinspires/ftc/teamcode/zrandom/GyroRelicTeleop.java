package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.util.EdgeDetector;

import java.util.Arrays;

/**
 * Created by Avery on 9/11/17.
 */
@Disabled
@TeleOp(name="Gyro RelicTeleOp", group="K9bot")
public class GyroRelicTeleop extends OpMode {

    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;
    EdgeDetector turnReq;
    ModernRoboticsI2cGyro snsGyro;
    double target = 0;
    double lastTheta = 0;
    double lastTime = 0;

    public void init() {
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");
        snsGyro.calibrate();
        while (snsGyro.isCalibrating()) {
            Thread.yield();
        }
        snsGyro.resetZAxisIntegrator();

        mtrFL.setDirection(DcMotorSimple.Direction.REVERSE);
        mtrFR.setDirection(DcMotorSimple.Direction.REVERSE);
        mtrBL.setDirection(DcMotorSimple.Direction.REVERSE);
        mtrBR.setDirection(DcMotorSimple.Direction.REVERSE);
        turnReq = new EdgeDetector();
    }

    @Override
    public void start() {
        lastTime = System.nanoTime();
    }

    @Override
    public void loop(){

        double theta = snsGyro.getIntegratedZValue();
        double now = System.nanoTime();

        //This controls the drive train
        double x1 = gamepad1.left_stick_x;
        double y1 = -gamepad1.left_stick_y;
        double x2 = gamepad1.right_stick_x;

        double frontLeft;
        double frontRight;
        double backLeft;
        double backRight;

        turnReq.update(x2 == 0);
        if (turnReq.isHigh())
            target = theta;

        if (x2 == 0 && Math.abs(theta - target) > 2) {
            // clockwise is negative
            x2 = Range.clip((theta - target) * 0.02, -1, 1);
        }

        frontLeft = (x1 - y1 - x2);
        frontRight = (x1 + y1 - x2);
        backLeft = (-x1 - y1 - x2);
        backRight = (-x1 + y1 - x2);

        double[] powers = {Math.abs(frontLeft), Math.abs(frontRight), Math.abs(backLeft), Math.abs(backRight)};
        Arrays.sort(powers);
        if (powers[3] > 1) {
            frontLeft /= powers[3];
            frontRight /= powers[3];
            backLeft /= powers[3];
            backRight /= powers[3];
        }

        mtrFL.setPower(Range.clip(frontLeft, -1, 1));
        mtrFR.setPower(Range.clip(frontRight, -1, 1));
        mtrBL.setPower(Range.clip(backLeft, -1, 1));
        mtrBR.setPower(Range.clip(backRight, -1, 1));
        telemetry.addData("fl", frontLeft);
        telemetry.addData("fr", frontRight);
        telemetry.addData("bl", backLeft);
        telemetry.addData("br", backLeft);
        telemetry.addData("gyro", theta);
        telemetry.addData("w", (theta - lastTheta) / (now / lastTime));
        lastTheta = theta;
        lastTime = System.nanoTime();
    }
}


