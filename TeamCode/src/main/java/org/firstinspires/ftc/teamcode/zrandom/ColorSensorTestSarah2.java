package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;

/**
 * Created by Sarahpambi76 on 10/22/17.
 */
@Disabled
@TeleOp(name = "ColorSensorTestSarah2")
public class ColorSensorTestSarah2 extends OpMode{

    // Color sensor definition
    ColorSensor16Bit snsColorB;
    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorT;

    //Drive train definitions
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;


    public void init() {

        //snsColorT = new ColorSensor16Bit(hardwareMap , "snsColorT", 0x3e);
        snsColorB = new ColorSensor16Bit(hardwareMap , "snsColorB", 0x3c);
        snsColorF = new ColorSensor16Bit(hardwareMap , "snsColorF", 0x3a);



        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");


        mtrFL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrFR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        int[] Values16B = snsColorB.ReadColor16();
        int redB = Values16B[0];
        int[] Values16F = snsColorF.ReadColor16();
        int redF = Values16F[0];
        //int[] Values16T = snsColorT.ReadColor16();
        //int redT = Values16T[0];

    }

    @Override
    public void loop() {
        int[] Values16B = snsColorB.ReadColor16();
        int redB = Values16B[0];
        int[] Values16F = snsColorF.ReadColor16();
        int redF = Values16F[0];
       // int[] Values16T = snsColorT.ReadColor16();
        //int redT = Values16T[0];


//<<<<<<< HEAD
//=====
        double frontLeft;
        double frontRight;
        double backLeft;
        double backRight;

        //gamepad definitions
        double x1 = gamepad1.left_stick_x;
        double y1 = -gamepad1.left_stick_y;
        double x2 = gamepad1.right_stick_x;

        //int array of color sensor values


        //This controls the drive train

        frontLeft = x1 - y1 - x2;
        frontRight = x1 + y1 - x2;
        backLeft = -x1 - y1 - x2;
        backRight = -x1 + y1 - x2;

        mtrFL.setPower(Range.clip(frontLeft, -1, 1));
        mtrFR.setPower(Range.clip(frontRight, -1, 1));
        mtrBL.setPower(Range.clip(backLeft, -1, 1));
        mtrBR.setPower(Range.clip(backRight, -1, 1));

///>>>>>>> 471e950140a62730a54de9258d1a4f0dcce27ecb

        //Read color sensors
       // telemetry.addData("ColorT = ", redT);
        telemetry.addData("ColorB = ", redB);
        telemetry.addData("ColorF = ", redF);
        telemetry.update();


    }

}
