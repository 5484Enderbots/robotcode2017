package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by guinea on 10/1/17.
 */
@Disabled
@TeleOp(name="encoder test", group="test")
public class USDigitalEncoderTest extends OpMode {
    DcMotor enc;
    @Override
    public void init() {
        enc = hardwareMap.dcMotor.get("enc1");
    }

    @Override
    public void loop() {
        telemetry.addData("value: ", enc.getCurrentPosition());
    }
}
