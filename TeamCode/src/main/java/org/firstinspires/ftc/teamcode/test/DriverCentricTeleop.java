package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.opmodes.odsSensorTest;

/**
 * Created by Avery on 1/18/18.
 */
@TeleOp(name="Driver centric teleop", group="test")
public class DriverCentricTeleop extends odsSensorTest {
    @Override
    public boolean driverCentric() {
        return true;
    }
}
