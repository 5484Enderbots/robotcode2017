package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by Sarahpambi76 on 11/10/17.
 */
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Avery on 11/10/17.
 */
@Disabled
@TeleOp(name="DemoTeleop", group="K9bot")
public class DemoTeleop extends OpMode {
    DcMotor mtrFR;
    DcMotor mtrBR;
    DcMotor mtrFL;
    DcMotor mtrBL;
    DcMotor Arm;
    DcMotor Extend;
    Servo svoGrab;
    public void init() {
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        Arm = hardwareMap.dcMotor.get("Arm");
        Extend = hardwareMap.dcMotor.get("Extend");
        svoGrab = hardwareMap.servo.get("svoGrab");
    }
    @Override
    public void loop(){
        Float LeftPower = gamepad1.left_stick_y;
        Float RightPower = gamepad1.right_stick_y;

        if (LeftPower < -0.5 || LeftPower > 0.5){
            mtrBL.setPower(LeftPower);
            mtrFL.setPower(LeftPower);
        }
        else{
            mtrBL.setPower(0);
            mtrFL.setPower(0);
        }
        if (RightPower < -0.5 || RightPower > 0.5){
            mtrBR.setPower(-RightPower);
            mtrFR.setPower(-RightPower);
        }
        else {
            mtrBR.setPower(0);
            mtrFR.setPower(0);
        }

        Float ArmPower = gamepad2.left_stick_y;
        Arm.setPower(ArmPower);

        Float ExtendPower = gamepad2.right_stick_y;
        Extend.setPower(ExtendPower);


        if (gamepad2.a){
            svoGrab.setPosition(0.5);
        }
        else {
            svoGrab.setPosition(0);
        }
    }
}
