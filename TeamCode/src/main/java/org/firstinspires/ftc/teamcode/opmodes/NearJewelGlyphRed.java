package org.firstinspires.ftc.teamcode.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

/**
 * Created by Avery on 10/22/17.
 */
@Disabled
@Autonomous (name = "NearJewelGlyphRed")
public class NearJewelGlyphRed extends NearJewelGlyphBlue {

    public boolean isBlueAlliance() {
        return false;
    }
}
