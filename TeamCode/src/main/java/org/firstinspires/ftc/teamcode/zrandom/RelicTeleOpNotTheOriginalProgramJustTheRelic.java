package org.firstinspires.ftc.teamcode.zrandom;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by Sarahpambi76 on 11/29/17.
 */
@Disabled
@TeleOp (name = "Relic")
public class RelicTeleOpNotTheOriginalProgramJustTheRelic extends OpMode{

    DcMotor relicRotation;
    DcMotor relicSlide;

    @Override
    public void init(){

    relicRotation = hardwareMap.dcMotor.get("relicRotation");
    relicSlide = hardwareMap.dcMotor.get("relicSlide");

        relicRotation.setTargetPosition(0);
        relicSlide.setTargetPosition(0);

    }
    @Override
    public void init_loop(){
        // Do hardware mapping


    }
    public void start(){

    }
    @Override
    public void loop(){



    }
    public void stop(){
        // Stop all motors

    }

}
