package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by Avery on 10/2/17.
 */
@Disabled
@Autonomous(name = "colorsensortestsarah")
public class ColorSensorTestSarah extends LinearOpMode {

    ColorSensor snsColorB;
    DcMotor FLmotor;
    DcMotor FRmotor;

    @Override
    public void runOpMode() {

        FLmotor = hardwareMap.dcMotor.get("mtrFL");
        FRmotor = hardwareMap.dcMotor.get("mtrFR");
        snsColorB = hardwareMap.colorSensor.get("snsColorB");

        FLmotor.setDirection(DcMotor.Direction.REVERSE);
        FRmotor.setDirection(DcMotor.Direction.REVERSE);




        telemetry.addLine(" Ready for start");
        telemetry.update();


        waitForStart();
        // Goes forward at 0.5 speed until robot sees grey floor
            while(snsColorB.red() < 30) {

                FLmotor.setPower(0.5);

            }
            FLmotor.setPower(0);

       // FLmotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
       // FRmotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            telemetry.addData("moving, color value is: ", snsColorB.red());
            telemetry.update();
    }
}
