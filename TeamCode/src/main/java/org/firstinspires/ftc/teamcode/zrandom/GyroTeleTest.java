package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by Avery on 10/22/17.
 */
@Disabled
@TeleOp (name = "GyroTeleTest", group = "K9bot")
public class GyroTeleTest extends OpMode {


    ModernRoboticsI2cGyro snsGyro;

    public void init() {

        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");

        telemetry.log().add("Gyro Calibrating. Do Not Move!");
        snsGyro.calibrate();

    }
    public void loop(){

        float gyroValue = snsGyro.getIntegratedZValue();
        telemetry.addData("Gyro heading:", gyroValue);
        telemetry.update();

    }

}
