package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;


/**
 * Created by bensterbenk on 10/2/17.
 */
@Disabled
@Autonomous

public class ColorTest extends LinearOpMode {
    ColorSensor snsColorB;

    @Override

    public void runOpMode() {

        snsColorB = hardwareMap.colorSensor.get("snsColorB");
        telemetry.addLine("Ready");
        telemetry.update();
        waitForStart();

        loop();
        snsColorB.red();

        while (snsColorB.red() > 30) {
            telemetry.addLine("moving");
            telemetry.update();

        }


    }
}
