package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
/**
 * Created by bensterbenk on 9/24/17.
 */
@Disabled
@Autonomous(name="Mechanumboi")

public class MechanumTest extends LinearOpMode {
    DcMotor FLmotor;
    DcMotor FRmotor;
    ColorSensor colorsensor;

    public void runOpMode(){
        FLmotor = hardwareMap.dcMotor.get("mtrFL");
        FRmotor = hardwareMap.dcMotor.get("mtrFR");
        colorsensor = hardwareMap.colorSensor.get("color");
        waitForStart();


        FLmotor.setDirection(DcMotor.Direction.REVERSE);
        FRmotor.setDirection(DcMotor.Direction.REVERSE);
        // Make it go forward until it sees the line.

        FLmotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        FRmotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        FLmotor.setTargetPosition(5000);
        FRmotor.setTargetPosition(5000);

        FLmotor.setPower(1);
        FRmotor.setPower(1);

        FRmotor.setDirection(DcMotor.Direction.FORWARD);
        FLmotor.setTargetPosition(3000);
        FRmotor.setTargetPosition(3000);

        while (colorsensor.blue() < 20) {
            FRmotor.setDirection(DcMotor.Direction.REVERSE);
            FLmotor.setTargetPosition(3000);
            FRmotor.setTargetPosition(3000);
        }
        telemetry.addData("Color Sensor", colorsensor.blue());
        telemetry.update();

    }



}
