package org.firstinspires.ftc.teamcode.opmodes;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.util.DriveBase;
import org.firstinspires.ftc.teamcode.util.Flipper;

/**
 * Created by Avery on 9/11/17.
 */
@TeleOp(name="SingleDriverOp", group="K9bot")
public class SingleDriverOp extends OpMode {

    TouchSensor LowLimmit;
    TouchSensor HighLimmit;
    boolean limmitHit = false;
    long limmitTime = System.nanoTime();
    public boolean LowStop = false;
    public boolean HighStop = false;
    public boolean DispenserMoving = false;

    TouchSensor snsRelic;
    OpticalDistanceSensor snsGlyph1;
    OpticalDistanceSensor snsGlyphDispenser;
    Servo svoRelicWrist;
    Servo svoRelicClaw;
    Servo svoRelicArm;

    Servo svoStone;
    Servo svoJewel;
    Servo svoColor;
    Servo svoPhone;
    DriveBase driveBase;
    DcMotor mtrCollector;
    Servo svoGlyph1;
    Servo svoGlyph2;
    Servo svoCollect;
    Servo orientationSVO;
    //Servo dispensingSVO;
    DcMotor mtrDispense;
    DcMotor mtrRelicExtend;
    BNO055IMU snsIMU;

    double offset = 0;
    //GyroSensor snsGyro;

    Flipper flipper;

    //DcMotor mtrAngle;
    int rotLeft = 1850;
    int rotUp = 0;
    int rotDown = 3000;
    double rotPower = 0.6;


    /**
     *
     * Constants for each servo position for the Relic Grabber, wrist, claw, and encoder positions for slide
     *
     */
    public double relicClawGrab = 1;
    public double relicClawRelease = 0.17647059;

    public double RelicAngleDown = 0.56862745;
    public double RelicAngleBack = 1;
    public double RelicAngleUp = 0.7843;
    public double RelicAngleIdle = 0.90196078;

    public double RelicWristFolded = 0.19607843;
    public double RelicWristOut = 0.17647059;

    double POS_JEWEL_UP = 0.7843;
    double POS_JEWEL_DOWN = 0.1176;
    double POS_COLOR_STRAIGHT = 22d/255d;
    double POS_COLOR_TOWARDS_RELIC = 0.6667;
    double POS_COLOR_AWAY_RELIC = 1.0;

    public int RelicSlideIn = 0;
    public int RelicSlideMid = -1920;
    public int RelicSlideOut = -3740;

    /**
     *
     * Constants for each servo position for the Relic Grabber, wrist, claw, and encoder positions for slide
     *
     */



    /**
     *
     * Constants for each servo position for the dispenser's orientation
     *
     */
    public double OrientY = 0.5254902;
    public double OrientX = 0.28235294;
    public double OrientB = 0.78823529;
    public double OrientA = 0.01568627;
    /**
     *
     * Constants for each servo position for the dispenser's orientation
     *
     */




    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */
    public double Release1 = 0.88;
    public double Idle1 = 0.21;//0.23529412;
    public double Grabbed1 = 0.03921569;

    public double Release2 = 1;
    public double Idle2 = 0.3;
    public double Grabbed2 = 0;
    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */




    public static final double LOW_POWER = 3;
    public static final double FAST_POWER = 1;
    public static final double NORMAL_POWER = 1.5;
    boolean xButton, xButtonPressed, xLatchState;

    public boolean Run_Collector = false;
    public boolean Stop_Collector = true;
    public boolean Slow_Collector = true;
    public boolean Fast_Collector = false;
    public boolean Reverse_Collector = false;
    public boolean isCollectorGlyph = false;

    boolean dispenserRaising = false;

    public double CollectorPower;
    public double anglePower = 0.4;
    public double extendPower = 0.67;
    public double retractPower = 0.4;

    ElapsedTime dispenserRuntime = new ElapsedTime();

    private double signedPow(double v, double v1) {
        return Math.copySign(Math.pow(v, v1), v);
    }

    public void init_loop() {
        // pass, if we need it, it can be used later. both init and init_loop get called during opmode init.
    }
    @Override
    public void init(){

        driveBase = new DriveBase(hardwareMap, false);

        //HardwareMaps
        orientationSVO = hardwareMap.servo.get("orientationSVO");

        svoRelicWrist = hardwareMap.servo.get("svoRelicWrist");
        svoRelicWrist.setPosition(RelicWristOut);

        svoRelicArm = hardwareMap.servo.get("svoRelicArm");
        svoRelicArm.setPosition(RelicAngleBack);

        svoRelicClaw = hardwareMap.servo.get("svoRelicClaw");
        svoRelicClaw.setPosition(relicClawGrab);
        snsGlyph1 = hardwareMap.get(OpticalDistanceSensor.class, "snsGlyph1");
        snsGlyphDispenser = hardwareMap.get(OpticalDistanceSensor.class, "snsGlyphDispenser");
        mtrDispense = hardwareMap.dcMotor.get("mtrDispense");
        mtrCollector = hardwareMap.dcMotor.get("mtrCollector");
        svoGlyph1 = hardwareMap.servo.get("svoGlyph1");
        svoGlyph2 = hardwareMap.servo.get("svoGlyph2");
        svoJewel = hardwareMap.servo.get("svoJewel");
        svoColor = hardwareMap.servo.get("svoColor");
        svoCollect = hardwareMap.servo.get("svoCollect");
        svoStone = hardwareMap.servo.get("svoStone");
        svoPhone = hardwareMap.servo.get("svoPhone");

        mtrRelicExtend = hardwareMap.dcMotor.get("mtrRelicExtend");
        mtrCollector.setDirection(DcMotorSimple.Direction.REVERSE);

        svoPhone.setPosition(0.8);
        svoStone.setPosition(0.11764706);
        svoCollect.setPosition(0.5);

        LowLimmit = hardwareMap.touchSensor.get("LowLimmit");
        HighLimmit = hardwareMap.touchSensor.get("HighLimmit");
        snsRelic = hardwareMap.touchSensor.get("snsRelic");
        //snsGyro = hardwareMap.gyroSensor.get("snsGyro");

        flipper = new Flipper(mtrDispense, LowLimmit, HighLimmit);
       // RelicLimmit = hardwareMap.touchSensor.get("RelicLimmit");
      //  mtrDispense.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        svoColor.setPosition(POS_COLOR_STRAIGHT);
        svoJewel.setPosition(POS_JEWEL_UP);

        if (driverCentric()) {
            BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
            parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
            parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
            parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
            parameters.loggingEnabled = true;
            parameters.loggingTag = "IMU";
            parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
            snsIMU = hardwareMap.get(BNO055IMU.class, "snsIMU");
            snsIMU.initialize(parameters);
        }

    }
    //Start is pressed
    public void start() {
        orientationSVO.setPosition(OrientA);
        CollectorPower = -0.7;
        svoStone.scaleRange(0, 1);
        svoRelicClaw.setPosition(relicClawRelease);
        svoRelicArm.setPosition(RelicAngleIdle);
        svoRelicWrist.setPosition(RelicWristOut);
        //svoJewel.setPosition(0.8627451);
    }
    @Override
    public void loop(){



        /**
        ||
        ||  CODE FOR PUSHING OUT THE COLLECTOR & BRINGING UP JEWEL ARM.
        ||
         */
        if (gamepad1.y){
            svoCollect.setPosition(0);
        }
        else {
            svoCollect.setPosition(0.5);
        }

        if (gamepad2.dpad_right) {
            svoJewel.setPosition(POS_JEWEL_UP);
            svoColor.setPosition(POS_COLOR_STRAIGHT);
        }
         /**
        ||
        ||  CODE FOR PUSHING OUT THE COLLECTOR & BRINGING UP JEWEL ARM.
        ||
         */




         /**
        ||
        ||  CODE FOR OPERATING DRIVETRAIN
        ||
         */
        // Factor to divide the powers by
        double powerdiv;
        //This controls the drive train
        double x1 = signedPow(gamepad1.left_stick_x, 2);
        double y1 = -signedPow(gamepad1.left_stick_y, 2);
        double x2 = signedPow(gamepad1.right_stick_x, 2);

        if (driverCentric()) {
            Orientation angles = snsIMU.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.RADIANS);
            telemetry.addData("z", angles.firstAngle);
            telemetry.addData("y", angles.secondAngle);
            telemetry.addData("x", angles.thirdAngle);
            if (gamepad1.start) {
                offset = (angles.firstAngle + Math.PI);
                //snsGyro.resetZAxisIntegrator();
            }
            // apply rotation matrix
            double angle = (angles.firstAngle + Math.PI) - offset;
            double x = x1 * Math.cos(angle) - y1 * Math.sin(angle);
            double y = x1 * Math.sin(angle) + y1 * Math.cos(angle);
            x1 = x; y1 = y;
        }

        powerdiv=NORMAL_POWER;
        driveBase.driveArcade(x1, y1, x2, powerdiv);
        /**
         ||
         ||  CODE FOR OPERATING DRIVETRAIN
         ||
         */




        /**
         ||
         ||  CODE FOR OPERATING THE DISPENSER
         ||
         */

       // OLD LOGIC, USED FOR ROCHESTER, BUT NOW WE WANT AN ACTUAL MIDDLE POSITION

        // if we need to change any of the constants, highlight the function name with the cursor
        // and then press control/cmd-b, then scroll up

       /**
        if (gamepad2.dpad_up) {
            flipper.up();
        } else if (gamepad2.dpad_down) {
            flipper.down();
            orientationSVO.setPosition(OrientA);
        } else if (gamepad2.dpad_right) {
            flipper.stop();
        }
        flipper.update(telemetry);
        /*
        **/
        if (gamepad1.dpad_up && !HighLimmit.isPressed()){
            mtrDispense.setPower(1);
        }else if (HighLimmit.isPressed() && !gamepad1.dpad_down){
            mtrDispense.setPower(0);}

        long limmitDiff = 0;
        if (LowLimmit.isPressed()) {
            if (!limmitHit)
                limmitTime = System.nanoTime();
            limmitDiff = System.nanoTime() - limmitTime;
        } else {
            limmitHit = false;
        }

        if (gamepad1.dpad_down && !LowLimmit.isPressed()){
            mtrDispense.setPower(-0.4);
            orientationSVO.setPosition(OrientA);
        }
        else if (LowLimmit.isPressed() && !gamepad1.dpad_up){
            mtrDispense.setPower(0);
        }
        if (gamepad1.dpad_right){mtrDispense.setPower(0);}


/*
       if (gamepad2.dpad_left){
            mtrDispense.setTargetPosition(rotLeft);
            mtrDispense.setPower(rotPower);
        }

        if (gamepad2.dpad_down && !LowStop){
            mtrDispense.setTargetPosition(rotDown);
            mtrDispense.setPower(rotPower);
        }

        if (gamepad2.dpad_up && !HighStop){
            mtrDispense.setTargetPosition(rotUp);
            mtrDispense.setPower(rotPower);
        }

        if (LowStop && !gamepad2.dpad_left && !gamepad2.dpad_up){mtrDispense.setPower(0);}
        if (HighStop && !gamepad2.dpad_left && !gamepad2.dpad_down){mtrDispense.setPower(0);}

        if (HighLimmit.isPressed()){HighStop = true;} else{HighStop = false;}
        if (LowLimmit.isPressed()){LowStop = true;} else{LowStop = false;}

        if (gamepad2.dpad_right){mtrDispense.setPower(0);}*/

        /**
         ||
         ||  CODE FOR OPERATING THE DISPENSER
         ||
         */



        /**
         ||
         ||  CODE FOR OPERATING THE BALANCING STONE AND THE ORIENTATION SERVO
         ||
         */

        if (gamepad1.b){svoStone.setPosition(0.67);}
        else if (gamepad1.x){svoStone.setPosition(0.11764706);}

        //Single press of A, B, X, and Y on gamepad 2 results in servo orientating angles of 0, 90, 180, and 270
        if (gamepad2.x) {orientationSVO.setPosition(OrientX);}

        if (gamepad2.y) {orientationSVO.setPosition(OrientY);}

        if (gamepad2.b) {orientationSVO.setPosition(OrientB);}

        if (gamepad2.a) {orientationSVO.setPosition(OrientA);}

        /**
         ||
         ||  CODE FOR OPERATING THE BALANCING STONE AND THE ORIENTATION SERVO
         ||
         */







        /**
         ||
         ||  CODE FOR OPERATING GLYPH GRABBERS
         ||
         */

        if (gamepad1.left_trigger > 0.5 && !gamepad1.left_bumper){
            svoGlyph1.setPosition(Grabbed1);
            svoGlyph2.setPosition(Grabbed2);
        }
        else if (gamepad1.left_bumper && !(gamepad1.left_trigger > 0.5)){
            svoGlyph1.setPosition(Idle1);
            svoGlyph2.setPosition(Idle2);
        }
        else if (gamepad1.left_bumper && gamepad1.left_trigger > 0.5){
            svoGlyph1.setPosition(Release1);
            svoGlyph2.setPosition(Release2);
        }

        if(snsGlyphDispenser.getRawLightDetected() > 0.15 && !gamepad1.left_bumper && !(gamepad1.left_trigger > 0.5)){
            svoGlyph1.setPosition(Grabbed1);
            svoGlyph2.setPosition(Grabbed2);
        }

        /**
         ||
         ||  CODE FOR OPERATING THE GLYPH GRABBERS
         ||
         */



        /**
         ||
         ||  CODE FOR OPERATING THE RELIC ANGLING, EXTENTION, AND GRABBING
         ||
         */

        /*if(gamepad2.left_stick_y < -0.5){
            mtrRelicExtend.setTargetPosition(RelicSlideOut);
            mtrRelicExtend.setPower(-1);
            svoRelicArm.setPosition(RelicAngleDown);
        }
        else if (gamepad2.left_stick_y > 0.5){
            mtrRelicExtend.setTargetPosition(RelicSlideIn);
            mtrRelicExtend.setPower(-1);
        }
        else if (gamepad2.left_stick_x < -0.5){
            mtrRelicExtend.setTargetPosition(RelicSlideMid);
            mtrRelicExtend.setPower(-1);
            svoRelicArm.setPosition(RelicAngleUp);
        }
        else if (gamepad2.left_stick_x > 0.5){
            mtrRelicExtend.setPower(0);
        }
        */
        if(gamepad2.left_stick_y < -0.5){
            mtrRelicExtend.setPower(-0.6);
        }
        else if(gamepad2.left_stick_y > 0.5){
            mtrRelicExtend.setPower(0.6);
        }
        else{
            mtrRelicExtend.setPower(0);
        }

        if(gamepad2.right_stick_y > 0.5){
            svoRelicArm.setPosition(RelicAngleIdle);
        }
        else if (gamepad2.right_stick_y < -.5){
            svoRelicArm.setPosition(RelicAngleDown);
        }
        else if(Math.abs(gamepad2.right_stick_x) > 0.5){
            svoRelicArm.setPosition(RelicAngleUp);
        }


        /**
        if (Math.abs(mtrRelicExtend.getCurrentPosition()) >20){
            svoRelicArm.setPosition(RelicAngleUp);
        }
        else if (Math.abs(mtrRelicExtend.getCurrentPosition()) > 1500){
            svoRelicArm.setPosition(RelicAngleDown);
        }
        else{
            svoRelicArm.setPosition(RelicAngleBack);
        }




        if(gamepad2.right_stick_y > 0.5){
            svoRelicWrist.setPosition(svoRelicWrist.getPosition() + 0.05);
        }
        else if(gamepad2.right_stick_y < -0.5){
            svoRelicWrist.setPosition(svoRelicWrist.getPosition() - 0.05);
        }

         **/
        /*
        if (gamepad2.right_stick_y < -0.5){
            svoRelicArm.setPosition(RelicAngleDown);
            svoRelicWrist.setPosition(RelicWristOut);}
        else if (gamepad2.right_stick_y > 0.5){
            svoRelicArm.setPosition(RelicAngleBack);
            svoRelicWrist.setPosition(RelicWristOut);}
        else if (Math.abs(gamepad2.right_stick_x) > 0.5){
            svoRelicArm.setPosition(RelicAngleUp);
            svoRelicWrist.setPosition(RelicWristFolded);}
            */



        if (gamepad2.right_trigger > 0.5){svoRelicClaw.setPosition(relicClawGrab);
        //svoRelicWrist.setPosition(RelicWristOut);
            }
        else if (gamepad2.right_bumper){
            svoRelicClaw.setPosition(relicClawRelease);
        }



        /**
         ||
         ||  CODE FOR OPERATING THE RELIC ANGLING, EXTENTION, AND GRABBING
         ||
         */



        /**
         ||
         ||  CODE FOR OPERATING COLLECTOR
         ||
         */

        double odsValue = snsGlyph1.getRawLightDetected();

        telemetry.addData("ods reading ", odsValue);
        telemetry.addData("dispenser ods reading ", snsGlyphDispenser.getRawLightDetected());

        if (gamepad1.back)
            driveBase.resetEncoders();

        telemetry.addLine("-------------------------");
        telemetry.addData("encFL", driveBase.encFL.getEncValue());
        telemetry.addData("encFR", driveBase.encFR.getEncValue());
        telemetry.addData("encBL", driveBase.encBL.getEncValue());
        telemetry.addData("encBR", driveBase.encBR.getEncValue());

        telemetry.update();



        if (gamepad1.right_bumper) {
            if (!xButton) {
                xLatchState = !xLatchState;
                xButton = !xButton;
            }
        }
        else {xButton = false;}


        /*if (!xLatchState && isCollectorGlyph) // basically if we've detected a glyph and the x button has been pressed to stop, take us out of glyph mode
            isCollectorGlyph = false;

*/
        if(Run_Collector && (snsGlyphDispenser.getRawLightDetected() > 0.15 || snsGlyph1.getRawLightDetected() > 0.08) && !gamepad1.right_bumper) {
            xLatchState = false;

        }

        if (xLatchState) {
            Run_Collector = true;
            Stop_Collector = false;
        }

        else {
            Run_Collector = false;
            Stop_Collector = true;
        }


        if (gamepad1.b){
            Fast_Collector = true;
            Slow_Collector = false;
            Reverse_Collector = false;
        }
        else if (gamepad1.a){
            Fast_Collector = false;
            Slow_Collector = true;
            Reverse_Collector = false;
        }

        /**if(gamepad1.right_bumper){
            mtrCollector.setPower(0.5);
        }
         **/
        else if (gamepad1.right_trigger > 0.5){
            mtrCollector.setPower(-0.5);
            Stop_Collector = false;
            Run_Collector = false;
        }


        if (Fast_Collector){
            CollectorPower = 0.7;
        }
        else if (Slow_Collector){
            CollectorPower = 0.7;
        }
        if (Reverse_Collector){
            CollectorPower = -0.7;
        }


        if (Run_Collector){
            mtrCollector.setPower(CollectorPower);
        }
        else if (Stop_Collector) {
            mtrCollector.setPower(0);
        }

        /**
         ||
         ||  CODE FOR OPERATING COLLECTOR
         ||
         */






    }

    public void stop() {

    }

    public boolean driverCentric() {
        return false;
    }

}
