package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by guinea on 11/12/17.
 */
@TeleOp(name="Motor test")
public class MotorTest extends OpMode {
    public DcMotor mtrFL;
    public DcMotor mtrFR;
    public DcMotor mtrBL;
    public DcMotor mtrBR;
    DcMotor mtrDispense;
    Servo svoJewel;
    boolean upLatch = false;
    boolean downLatch = false;
    @Override
    public void init() {
        // left is 0
        // right is 42
        // middle is 21
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        hardwareMap.servo.get("svoJewel").setPosition(0.1176);
        svoJewel = hardwareMap.servo.get("svoColor");
        svoJewel.setPosition(127/255);
        /*
        mtrDispense = hardwareMap.dcMotor.get("mtrDispense");
        mtrDispense.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        mtrDispense.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        */
    }

    @Override
    public void loop() {
        //telemetry.addData("target", mtrDispense.getTargetPosition());
        //telemetry.addData("current", mtrDispense.getCurrentPosition());
        telemetry.addData("current", svoJewel.getPosition() * 255);
        telemetry.addData("currentRaw", svoJewel.getPosition());

        //mtrDispense.setPower(0.5);
        if (!gamepad1.dpad_up && upLatch) {
            svoJewel.setPosition((svoJewel.getPosition() * 255 + 1) / 255d);
            //mtrDispense.setTargetPosition(mtrDispense.getTargetPosition() + 10);
        }
        upLatch = gamepad1.dpad_up;

        if (!gamepad1.dpad_down && downLatch) {
            svoJewel.setPosition((svoJewel.getPosition() * 255 - 1) / 255d);
            //mtrDispense.setTargetPosition(mtrDispense.getTargetPosition() - 10);
        }
        downLatch = gamepad1.dpad_down;

        //if (gamepad1.x)
            //mtrDispense.setTargetPosition(150);
        /*
        FL is currently BL
        FR is right (negated?)
        BL is currently BR (negated?)
        BR is FL
         */
    }
}
