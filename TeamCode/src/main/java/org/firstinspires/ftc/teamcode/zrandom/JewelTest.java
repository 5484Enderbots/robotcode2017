package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;

/**
 * Created by bensterbenk on 10/19/17.
 */
@Disabled
@Autonomous(name="jeweltest")
public class JewelTest extends LinearOpMode{
    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorB;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;

    @Override
    public void runOpMode() {
        snsColorF = new ColorSensor16Bit(hardwareMap , "snsColorF", 0x3a);
        snsColorB = new ColorSensor16Bit(hardwareMap , "snsColorB", 0x3c);
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        telemetry.addLine(" Ready for start");
        telemetry.update();
        waitForStart();
        int[] Values16 = snsColorB.ReadColor16();
        int red = Values16[0];
    }
}
