package org.firstinspires.ftc.teamcode.test;

import android.os.Environment;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by guinea on 2/21/18.
 */
@Disabled
@TeleOp(name="andoroid test")
public class AndroidTest extends OpMode {
    @Override
    public void init() {

    }

    @Override
    public void loop() {
        telemetry.addLine(hardwareMap.appContext.getFilesDir().getAbsolutePath());
        telemetry.addLine(Environment.getDataDirectory().getAbsolutePath());

    }
}
