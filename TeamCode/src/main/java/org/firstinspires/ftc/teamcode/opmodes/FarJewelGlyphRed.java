package org.firstinspires.ftc.teamcode.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

/**
 * Created by Avery on 10/22/17.
 */
@Disabled
@Autonomous (name = "FarJewelGlyphRed")
public class FarJewelGlyphRed extends FarJewelGlyphBlue {

    public boolean isBlueAlliance() {
        return false;
    }
}
