package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;

import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;
import org.firstinspires.ftc.teamcode.util.Encoder;
import org.firstinspires.ftc.teamcode.vision.VuMarkReader;

/**
 * Created by Avery on 10/22/17.
 */
@Autonomous (name = "GLYPH LINEUP TEST")
public class GlyphLineUpTest extends LinearOpMode {


    /**
     *
     * Constants for each servo position for the dispenser's orientation
     *
     */
    public double OrientY = 0.5254902;
    public double OrientX = 0.28235294;
    public double OrientB = 0.78823529;
    public double OrientA = 0.01568627;
    /**
     *
     * Constants for each servo position for the dispenser's orientation
     *
     */


    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */
    public double Release1 = 0.88;
    public double Idle1 = 0.23529412;
    public double Grabbed1 = 0.03921569;

    public double Release2 = 1;
    public double Idle2 = 0.45098039;
    public double Grabbed2 = 0.2745098;
    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */

    public boolean multiglyph = false;

    TouchSensor snsProx1;
    TouchSensor snsProx2;

    ColorSensor16Bit snsColorF;
    //ColorSensor16Bit snsColorJ;
    // ModernRoboticsI2cColorSensor snsColorJewel;
    ColorSensor16Bit snsColorJewel;
    ModernRoboticsI2cGyro snsGyro;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;
    Encoder encFL;
    Servo svoJewel;
    Servo svoStone;
    DcMotor mtrCollector;
    Servo svoGlyph1;
    Servo svoGlyph2;
    Servo svoCollect;
    Servo orientationSVO;
    DcMotor mtrDispense;
    TouchSensor snsAllignD;
    TouchSensor snsAllignR;
    TouchSensor snsCollector;
    //Servo dispensingSVO;

    @Override
    public void runOpMode() {

        orientationSVO = hardwareMap.servo.get("orientationSVO");
        orientationSVO.setPosition(OrientX);
        svoCollect = hardwareMap.servo.get("svoCollect");
        svoCollect.setPosition(0.5);
        svoJewel = hardwareMap.servo.get("svoJewel");
        svoJewel.setPosition(0.8);

        snsAllignD = hardwareMap.touchSensor.get("snsAllignD");
        snsAllignR = hardwareMap.touchSensor.get("snsAllignR");

        snsProx1 = hardwareMap.touchSensor.get("snsProx1");
        snsProx2 = hardwareMap.touchSensor.get("snsProx2");

        svoGlyph1 = hardwareMap.servo.get("svoGlyph1");
        svoGlyph2 = hardwareMap.servo.get("svoGlyph2");
        svoGlyph1.setPosition(Release1);
        svoGlyph2.setPosition(Release2);

        mtrDispense = hardwareMap.dcMotor.get("mtrDispense");
        mtrDispense.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        // do the vuforia stuff
        RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.UNKNOWN;
        VuMarkReader vuMarkReader = new VuMarkReader(hardwareMap.appContext);
        vuMarkReader.start();

        mtrCollector = hardwareMap.dcMotor.get("mtrCollector");
        snsCollector = hardwareMap.touchSensor.get("snsCollector");

        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");

        svoStone = hardwareMap.servo.get("svoStone");
        svoStone.scaleRange(0, 1);
        svoStone.setPosition(0.17647);

        encFL = new Encoder(mtrFL);

        mtrFL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrFR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");

        snsColorF = new ColorSensor16Bit(hardwareMap, "snsColorF", 0x3a);

        snsColorJewel = new ColorSensor16Bit(hardwareMap, "snsColorJewel", 0x38);
        snsGyro.resetZAxisIntegrator();

        snsGyro.calibrate();

        /*
            so, you see, the problem with waitForStart(); is that we need to continuously poll the
            vumark reader in a loop to get the vumark, right? the problem with that, however, is that
            waitForStart(); basically pauses the program until the play button is pressed. so we do a workaround:
        */

        while (!isStarted() && !isStopRequested()) { // basically loops until the play button is pressed

            // only record a seen vumark if its actually useful:
            RelicRecoveryVuMark v = vuMarkReader.getVuMark();
            if (v != RelicRecoveryVuMark.UNKNOWN)
                vuMark = v;

            // give us some details on the status of the vumark and the gyro sensor
            telemetry.addData("Gyro calibrating: ", snsGyro.isCalibrating());
            telemetry.addData("VuMark", vuMark.toString());
            telemetry.update();
        }

        // shut down the vumark reader as the opmode starts
        vuMarkReader.stop();

        if (isStopRequested()) return;

        snsGyro.resetZAxisIntegrator();

        svoGlyph1.setPosition(Idle1);
        svoGlyph2.setPosition(Grabbed2);

        snsGyro.resetZAxisIntegrator();
        svoGlyph2.setPosition(Grabbed2);
        PlaceFirstGlyph(vuMark);
        //PickSecondGlyph();
        //PlaceGlyph();
        stop();
    }

    public void DrivePower(double fr, double fl, double br, double bl) {
        mtrBL.setPower(bl);
        mtrBR.setPower(br);
        mtrFL.setPower(fl);
        mtrFR.setPower(fr);
    }
    public void SenseJewel() {
        svoJewel.setPosition(0.04);
        sleep(2000);
        int [] JewelColors = snsColorJewel.ReadColor16();
        int blueJ = JewelColors [2];
        int RedJ = JewelColors [0];
        int ColorCompare = blueJ - RedJ;
        if ((ColorCompare > 0 && isBlueAlliance()) || (ColorCompare < 0 && !isBlueAlliance())) {
            encFL.resetEncoder();
            while (encFL.getEncValue() > -90&& opModeIsActive()) {
                DrivePower(-0.1, -0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            svoJewel.setPosition(1);
            encFL.resetEncoder();
            while (encFL.getEncValue() < 90&& opModeIsActive()) {
                DrivePower(0.1, 0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);

        } else {
            encFL.resetEncoder();
            while (encFL.getEncValue() < 90&& opModeIsActive()) {
                DrivePower(0.1, 0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            svoJewel.setPosition(1);
            encFL.resetEncoder();
            while (encFL.getEncValue() > -90&& opModeIsActive()) {
                DrivePower(-0.1, -0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
        }
    }
    public void PickSecondGlyph() {
        /**
         * Prepares for picking next glyph by flipping out collector and moving forward to avoid hitting cryptobox
         */
        multiglyph = true;
        svoGlyph2.setPosition(Idle2);
        encFL.resetEncoder();
        while (encFL.getEncValue() < 500) {
            DrivePower(-0.4, 0.4, -0.4, 0.4);
        }
        DrivePower(0, 0, 0, 0);
        mtrDispense.setTargetPosition(1850);
        orientationSVO.setPosition(OrientA);
        mtrDispense.setPower(0.6);
        svoCollect.setPosition(0);
        sleep(2500);
        svoCollect.setPosition(1);
        mtrDispense.setTargetPosition(-1120);
        mtrDispense.setPower(0.7);
        sleep(1500);
        svoCollect.setPosition(0.5);

        /**
         * Drives toward the center of the glyph pit a little before the dance to grab glyphs
         */
        encFL.resetEncoder();
        DrivePower(-0.36, 0.36, -0.36, 0.36);
        mtrCollector.setPower(0.5);
        sleep(800);
        DrivePower(0, 0, 0, 0);
        sleep(200);
        /**
         * reset's gyro, and does a tank turn on opposite sides until the touch sensor in the collector picks up a glyph
         */
        snsGyro.resetZAxisIntegrator();
        long ts = System.nanoTime();
        while (snsCollector.isPressed() && opModeIsActive()) {
            DrivePower(-0.3, 0, -0.3, 0);
            sleep(500);
            DrivePower(0, 0.3, 0, 0.3);
            sleep(500);
            if (System.nanoTime() - ts > 4e9)
                break;
        }
        mtrCollector.setPower(0);
        /**
         * takes how much the robot is off by from the turn's and adjusts to get back to 0 so we're straightened out
         */
        int currentHeading = snsGyro.getIntegratedZValue();
        int targetHeading = 0;

        while (currentHeading > targetHeading && opModeIsActive()) {
            DrivePower(-0.2, -0.2, -0.2, -0.2);
            currentHeading = Math.abs(snsGyro.getIntegratedZValue());
        }
        DrivePower(0, 0, 0, 0);
        /**
         * backs away from glyph pit, and jogs the collector to account for 45 degree angle glyphs
         */
        encFL.resetEncoder();
        while (Math.abs(encFL.getEncValue()) < 500 && opModeIsActive()){
            DrivePower(-0.36, 0.36, -0.36, 0.36);
        }
        DrivePower(0, 0, 0, 0);
        mtrCollector.setPower(0.5);
        sleep(500);
        mtrCollector.setPower(-0.5);
        sleep(500);
        mtrCollector.setPower(0.5);
        sleep(500);
        mtrCollector.setPower(-0.6);
        sleep(200);
        mtrCollector.setPower(0.7);
        sleep(1000);
        DrivePower(0.4, -0.4, 0.4, -0.4);
        sleep(400);
        mtrCollector.setPower(0);
        DrivePower(0, 0, 0, 0);
        /**
         * grabs glyphs and heads back to cryptobox for placing the glyph
         */
        svoGlyph1.setPosition(Grabbed1);
        svoGlyph2.setPosition(Grabbed2);
        mtrDispense.setTargetPosition(0);
        mtrDispense.setPower(0.4);
        encFL.resetEncoder();
        while (encFL.getEncValue() > (-120) && opModeIsActive()) {
            DrivePower(-0.2, -0.2, 0.2, 0.2);
        }
        DrivePower(0, 0, 0, 0);
        mtrCollector.setPower(0);
        ts = System.nanoTime();
        encFL.resetEncoder();
        mtrCollector.setPower(-0.5);
        while((!snsAllignR.isPressed()  || !snsAllignD.isPressed()) && opModeIsActive()){
            if (System.nanoTime() - ts > 2.5e9) // 2.5 seconds
                break;
            DrivePower(0.4, -0.4, 0.4, -0.4);
        }
        DrivePower(0, 0, 0, 0);
        mtrCollector.setPower(0);
    }
    public void PlaceFirstGlyph(RelicRecoveryVuMark vuMark) {
        telemetry.setAutoClear(false);
        if ((vuMark == RelicRecoveryVuMark.LEFT)) {// && isBlueAlliance()) || (vuMark == RelicRecoveryVuMark.RIGHT && !isBlueAlliance())) {
            // left code

            /**
             * lets the driver know which column it's going for
             */
            telemetry.addLine("LEFT SIDE BOYS");
            telemetry.update();

            /**
             * Robot backs up 30 encoder counts from the cryptobox
             */
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
            }
            DrivePower(0, 0, 0, 0);
            sleep(300);
            /**
             * Robot strafes to column, left in this case, looking for reading on the proximity sensor or encoder counts as a backup
             */
            encFL.resetEncoder();
            while (Math.abs(encFL.getEncValue()) < (45) && opModeIsActive()){
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (80) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
                mtrDispense.setTargetPosition(1000);
                mtrDispense.setPower(0.5);
            }
            DrivePower(0, 0, 0, 0);
            orientationSVO.setPosition(OrientA);
            sleep(1000);
            svoGlyph1.setPosition(Release1);
            svoGlyph2.setPosition(Release2);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0 ,0);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-150) && opModeIsActive()){
                DrivePower( 0.35, -0.35, 0.35, -0.35);
            }
            DrivePower(0, 0, 0, 0);


        } else if ((vuMark == RelicRecoveryVuMark.RIGHT)) {// && isBlueAlliance()) || (vuMark == RelicRecoveryVuMark.LEFT && !isBlueAlliance())) {
            // right code


            /**
             * lets the driver know which column it's going for
             */
            telemetry.addLine("RIGHT SIDE BOYS");
            telemetry.update();

            /**
             * Robot backs up 30 encoder counts from the cryptobox
             */
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
            }
            DrivePower(0, 0, 0, 0);
            sleep(300);
            /**
             * Robot strafes to column, left in this case, looking for reading on the proximity sensor or encoder counts as a backup
             */
            encFL.resetEncoder();
            while (Math.abs(encFL.getEncValue()) < (60) && opModeIsActive()){
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (80) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
                mtrDispense.setTargetPosition(1000);
                mtrDispense.setPower(0.5);
            }
            DrivePower(0, 0, 0, 0);
            orientationSVO.setPosition(OrientA);
            sleep(1000);
            svoGlyph1.setPosition(Release1);
            svoGlyph2.setPosition(Release2);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0 ,0);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-150) && opModeIsActive()){
                DrivePower( 0.35, -0.35, 0.35, -0.35);
            }
            DrivePower(0, 0, 0, 0);
        } else {
            // center code


            /**
             * lets the driver know which column it's going for
             */
            telemetry.addLine("CENTER BOYS");
            telemetry.update();

            /**
             * Robot backs up 30 encoder counts from the cryptobox
             */
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
            }
            DrivePower(0, 0, 0, 0);
            sleep(300);
            /**
             * Robot strafes to column, left in this case, looking for reading on the proximity sensor or encoder counts as a backup
             */
            encFL.resetEncoder();
            while (Math.abs(encFL.getEncValue()) < (45) && opModeIsActive()){
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (80) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
                mtrDispense.setTargetPosition(1000);
                mtrDispense.setPower(0.5);
            }
            DrivePower(0, 0, 0, 0);
            orientationSVO.setPosition(OrientA);
            sleep(1000);
            svoGlyph1.setPosition(Release1);
            svoGlyph2.setPosition(Release2);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0 ,0);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-150) && opModeIsActive()){
                DrivePower( 0.35, -0.35, 0.35, -0.35);
            }
            DrivePower(0, 0, 0, 0);
        }
    }
    public void PlaceGlyph(){
        /**
         * this method assumes that the column doesn't matter.
         * We'd use this for placing glyphs OTHER than the first glyph.
         * Used in situations where what column you put it in wont get any extra points.
         * Will use for multiglyph auto for additional glyphs.
         */
        encFL.resetEncoder();
        while (encFL.getEncValue() < (500) && opModeIsActive()) {
            DrivePower(-0.35, 0.35, -0.35, 0.35);
        }
        DrivePower(0, 0, 0, 0);

        mtrDispense.setTargetPosition(1850);
        mtrDispense.setPower(0.4);
        sleep(300);
        encFL.resetEncoder();
        while (encFL.getEncValue() > (-80) && opModeIsActive()) {
            DrivePower(-0.3, -0.3, 0.3, 0.3);
        }
        DrivePower(0, 0, 0, 0);
        orientationSVO.setPosition(OrientA);
        sleep(700);

        encFL.resetEncoder();
        while (encFL.getEncValue() > (-230) && opModeIsActive()) {
            DrivePower(0.1, -0.1, 0.1, -0.1);
        }
        DrivePower(0, 0, 0, 0);
        svoGlyph1.setPosition(Release1);
        svoGlyph2.setPosition(Release2);

        sleep(1000);
        DrivePower(0.6, -0.6, 0.6, -0.6);
        sleep(1500);
        DrivePower(0, 0, 0, 0);

        encFL.resetEncoder();
        while (encFL.getEncValue() < 200 && opModeIsActive()) {
            DrivePower(-0.3, 0.3, -0.3, 0.3);
        }
        DrivePower(0, 0, 0, 0);


        mtrDispense.setTargetPosition(1850);
        mtrDispense.setPower(0.4);

        if (!multiglyph){
            svoCollect.setPosition(0);
            sleep(2000);
            svoCollect.setPosition(1);
            sleep(1500);
            svoCollect.setPosition(0.5);
        }

    }
    public boolean isBlueAlliance() {
        return true;
    }
}
