package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;
import org.firstinspires.ftc.teamcode.util.Encoder;

/**
 * Created by Avery on 10/2/17.
 */
@Disabled
@Autonomous(name = "AutonomousEncoder")
public class OofAuto extends LinearOpMode {

    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorB;
    ColorSensor16Bit snsColorT;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;
    ModernRoboticsI2cGyro snsGyro;
    Encoder encFL;



    @Override
    public void runOpMode() {
        snsColorF = new ColorSensor16Bit(hardwareMap , "snsColorF", 0x3a);
        snsColorB = new ColorSensor16Bit(hardwareMap , "snsColorB", 0x3c);
        snsColorT = new ColorSensor16Bit(hardwareMap , "snsColorT", 0x3e);
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        encFL = new Encoder(mtrFL);

        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");
        snsGyro.calibrate();
        telemetry.log().add("Do not move, Gyro is calibrating");


        telemetry.update();
        waitForStart();

        int[] Values16B = snsColorB.ReadColor16();
        int redB = Values16B[0];
        int[] Values16F = snsColorF.ReadColor16();
        int redF = Values16F[0];
        int[] Values16T = snsColorT.ReadColor16();
        int redT = Values16T[0];
        float gyroValue = snsGyro.getIntegratedZValue();

        DrivePower(0.1, -0.1, 0.1, -0.1);
        encFL.resetEncoder();
        while(redB < 10) {
            Values16B = snsColorB.ReadColor16();
            redB = Values16B[0];
            DrivePower(0.1, -0.1, 0.1, -0.1);

        }
        DrivePower(0,0,0,0);
        sleep(100);
        encFL.resetEncoder();
        /* while (gyroValue > -90){
            DrivePower(-0.1, 0.1, -0.1, 0.1);
        }
        while (redB < 20){
            Values16B = snsColorB.ReadColor16();
            redB = Values16B[0];
            DrivePower(0.1,-0.1,0.1,-0.1);
        }
        DrivePower(0,0,0,0);
        */






    }
    public void DrivePower( double fl, double fr, double bl, double br){
        mtrFL.setPower(fl);
        mtrFR.setPower(fr);
        mtrBL.setPower(bl);
        mtrBR.setPower(br);
    }
}