package org.firstinspires.ftc.teamcode.test;

import android.annotation.SuppressLint;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.DriveBase;

/**
 * Created by guinea on 11/22/17.
 */
@SuppressLint("DefaultLocale") // there's only like 2 phones in the world that runs this code, and they speak the english
@Disabled
@TeleOp(name="Sensor teleop")
public class SensorTeleop extends OpMode {
    DriveBase driveBase;
    @Override
    public void init() {
        driveBase = new DriveBase(hardwareMap, false);
    }

    @Override
    public void loop() {

        if (gamepad1.x)
            driveBase.resetDrive();

        // Factor to divide the powers by
        double powerdiv = 1;
        if (gamepad1.left_bumper) powerdiv = 2;
        if (gamepad1.left_trigger > 0.5) powerdiv = 4;

        // Update drivetrain motor powers
        driveBase.driveArcade(gamepad1.left_stick_x, -gamepad1.left_stick_y, gamepad1.right_stick_x, powerdiv);

        telemetry.addLine("FL FR");
        telemetry.addLine("BL BR");
        telemetry.addLine("----- Encoders -----");
        telemetry.addLine(String.format("%d  - %d", driveBase.encFL.getEncValue(), driveBase.encFR.getEncValue()));
        telemetry.addLine(String.format("%d  - %d", driveBase.encBL.getEncValue(), driveBase.encBR.getEncValue()));
        telemetry.addLine("----- Powers -----");
        telemetry.addLine(String.format("%.3f  - %.3f", driveBase.mtrFL.getPower(), driveBase.mtrFR.getPower()));
        telemetry.addLine(String.format("%.3f  - %.3f", driveBase.mtrBL.getPower(), driveBase.mtrBR.getPower()));


    }
}
