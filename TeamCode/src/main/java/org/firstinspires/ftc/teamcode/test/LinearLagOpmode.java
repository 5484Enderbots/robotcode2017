package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by guinea on 8/18/17.
 */
@Disabled
@TeleOp(name = "Linear lag opmode", group="test")
public class LinearLagOpmode extends LinearOpMode {
    long[] times;
    long counter;
    long last;
    final int samples = 10; // number of samples to average
    @Override
    public void runOpMode() throws InterruptedException {
        times = new long[samples];
        counter = 0;
        last = System.nanoTime();
        // set thread to max priority
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        waitForStart();
        while (opModeIsActive()) {
            if (counter % samples == 0) {
                long sum = 0;
                for (long i : times) {
                    sum += i;
                }
                telemetry.addData("avg_lag: ", sum / samples);
                telemetry.update();
            }
            long now = System.nanoTime();
            times[(int) (counter % samples)] = now - last;
            last = now;
            counter++;
        }
    }
}
