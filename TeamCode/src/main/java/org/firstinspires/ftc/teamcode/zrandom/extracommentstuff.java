package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.ServoController;
import com.qualcomm.robotcore.util.ElapsedTime;




/**
 * Created by Sarahpambi76 on 11/21/17.
 */

@Disabled
@TeleOp(name = "commentstuff", group = "MRI")  // @Autonomous(...) is the other common choice
public class extracommentstuff extends LinearOpMode {


    private ElapsedTime runtime = new ElapsedTime();

    Servo servo;   //Create an instance of Servo
    double target = 0.5;   //create a variable for the target value. This will initially be 0.5.


    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        servo = hardwareMap.servo.get("left");   //Our servo will be called "left" in the configuration file.

        servo.setPosition(target);  //Initialize position
        //status();                   //Display the position and PWM mode


        waitForStart();
        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {


            while (!gamepad1.a && !gamepad1.b) {     //This loop will repeat until a or b is pressed

                servo.getController().pwmDisable();  //Disable servos for 1 second
                status();
                if (!gamepad1.a && !gamepad1.b)
                    sleep(1000);

                servo.getController().pwmEnable();   //Enable servos for 1 second
                status();
                if (!gamepad1.a && !gamepad1.b)
                    sleep(1000);
            }


            while (!gamepad1.x) {     //This loop will repeat until x is pressed
                if (gamepad1.a) {     //Enable servos when a is pressed
                    servo.getController().pwmEnable();
                    status();
                }
                if (gamepad1.b) {     //Disable servos when b is pressed
                    servo.getController().pwmDisable();
                    status();
                }

                if (gamepad1.right_stick_x != 0) {

                    //update target value proportionally using a joystick.
                    target = 0.5 * gamepad1.right_stick_x + 0.5;

                    telemetry.addData("Position: ", target);
                    telemetry.addData("GamePad 1 right stick", gamepad1.right_stick_x);
                    telemetry.update();

                    if (target > 1)  //Ensure the target is between 0 and 1.
                        target = 1;
                    if (target < 0)
                        target = 0;

                    servo.setPosition(target);  //Set position. This will also enable all six servo ports.
                    status();

                }
            }

        }
    }


    void status() {
        if (servo.getController().getPwmStatus() == ServoController.PwmStatus.DISABLED) {
            telemetry.addData("PWM", "Disabled");
            telemetry.addData("Target", target);
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.update();
        } else {
            telemetry.addData("PWM", "Enabled");
            telemetry.addData("Target", target);
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.update();
        }
    }
}

    /*waitForStart();

    // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {


        float x = gamepad1.right_stick_x;
        //update target value proportionally using a joystick.
        target = 0.5 * x + 0.5;

        telemetry.addData("PositionX: ", target);
        telemetry.addData("GamePad 1 right stick", x);
        telemetry.update();

        servo.setPosition(target);  //Set position. This will also enable all six servo ports.

        float cr = gamepad2.right_stick_x;
        targetCR = 0.5 * cr + 0.5;

        telemetry.addData("PositionCR", targetCR);
        telemetry.addData("Gamepad 2 right stick", cr);
        telemetry.update();

        servoCR.setPosition(targetCR);

        sleep(40);

    }*/