package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.util.Encoder;
import org.firstinspires.ftc.teamcode.util.IniConfig;
import org.firstinspires.ftc.teamcode.util.PIDController;

import java.io.File;

/**
 * Created by guinea on 1/21/18.
 */
@TeleOp(name="Flipper test", group="test")
public class FlipperTest extends OpMode {
    class Flipper {
        IniConfig pidConfig;
        IniConfig.ConfigSection cfg;
        DcMotor mtrDispense;
        Encoder enc;
        TouchSensor lowLimmit;
        TouchSensor highLimmit;
        PIDController pid;
        boolean openLoop = true;
        boolean goingUp = false;
        boolean goingDown = false;
        int upTarget = 3000;
        int downTarget = 0;

        public Flipper(DcMotor mtrDispense, TouchSensor lowLimmit, TouchSensor highLimmit) {
            pidConfig = new IniConfig(new File("/sdcard/FIRST/pid.ini"));
            pidConfig.readConfig();
            cfg = pidConfig.getSection("Flipper");
            this.mtrDispense = mtrDispense;
            this.lowLimmit = lowLimmit;
            this.highLimmit = highLimmit;
            pid = new PIDController(cfg.getd("p"), cfg.getd("i"), cfg.getd("d"));
            upTarget = cfg.geti("upTarget", 3000);
            openLoop = true;
        }
        public void up() {
            goingUp = true;
            goingDown = false;
        }

        public void down() {
            goingUp = false;
            goingDown = true;
        }

        public void stop() {
            goingUp = goingDown = false;
        }
        public void update(Telemetry telemetry) {
            telemetry.addData("openLoop", openLoop);
            if (!openLoop)
                telemetry.addData("enc", enc.getEncValue());
            telemetry.addData("power", mtrDispense.getPower());
            // periodic actions here
            // this needs to be called every round of loop() in an opmode
            if (lowLimmit.isPressed() && openLoop) {
                enc = new Encoder(mtrDispense);
                enc.resetEncoder();
                openLoop = false;
            }
            // take us out of an invalid state
            if (goingUp && goingDown)
                goingUp = goingDown = false;
            // nothing to do, just stop
            if ((goingUp && highLimmit.isPressed()) || (goingDown && lowLimmit.isPressed()) || !(goingUp || goingDown)) {
                mtrDispense.setPower(0);
                return;
            }

            if (openLoop) {
                mtrDispense.setPower((goingUp) ? 0.4 : -0.25);
            }
            else {
                int target = (goingUp) ? upTarget : downTarget;
                mtrDispense.setPower(Range.clip(pid.get(target - enc.getEncValue()), -1, 1));
            }
        }


    }

    Flipper flipper;
    public void init() {
        flipper = new Flipper(hardwareMap.dcMotor.get("mtrDispense"), hardwareMap.touchSensor.get("LowLimmit"), hardwareMap.touchSensor.get("HighLimmit"));
    }

    public void loop() {
        if (gamepad1.dpad_up) {
            flipper.up();
        } else if (gamepad1.dpad_down) {
            flipper.down();
        } else {
            flipper.stop();
        }
        flipper.update(telemetry);
        telemetry.update();
    }

}
