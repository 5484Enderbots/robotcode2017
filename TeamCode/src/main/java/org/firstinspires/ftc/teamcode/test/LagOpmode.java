package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by guinea on 8/18/17.
 */
@Disabled
@TeleOp(name="Lag opmode", group="test")
public class LagOpmode extends OpMode {
    long[] times;
    long counter;
    long last;
    final int samples = 10; // number of samples to average
    @Override
    public void init() {
        times = new long[samples];
        counter = 0;
    }

    @Override
    public void start() {
        last = System.nanoTime();
        // set thread to max priority
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        (new Thread(){
            @Override
            public void run() {
                throw new RuntimeException("dab on the haters");
            }
        }).start();
    }

    @Override
    public void loop() {
        if (counter % samples == 0) {
            // push to telemetry the average of the last set amount of samples
            long sum = 0;
            for (long i : times) {
                sum += i;
            }
            telemetry.addData("avg_lag: ", sum / samples);
        }
        long now = System.nanoTime();
        times[(int) (counter % samples)] = now - last;
        last = now;
        counter++;
    }
}
