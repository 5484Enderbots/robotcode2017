package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;

/**
 * Created by Avery on 10/2/17.
 */
@Disabled
@Autonomous(name = "ParkAuto")
public class ParkAutoRelic extends LinearOpMode {

    ColorSensor16Bit snsColorB;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;


    @Override
    public void runOpMode() {

        snsColorB = new ColorSensor16Bit(hardwareMap , "snsColorB", 0x3c);
        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        telemetry.addLine(" Ready for start");
        telemetry.update();
        waitForStart();

            int[] Values16 = snsColorB.ReadColor16();
            int red = Values16[0];

            while(red > 30) {
                Values16 = snsColorB.ReadColor16();
                red = Values16[0];
                mtrFR.setPower(0.5);
                mtrFL.setPower(-0.5);
                mtrBR.setPower(0.5);
                mtrBL.setPower(-0.5);
                telemetry.addData("moving, color value is: ", String.format("%d", Values16[0]));
                telemetry.update();
            }
            mtrFR.setPower(0);
            mtrFL.setPower(0);
            mtrBR.setPower(0);
            mtrBL.setPower(0);
            telemetry.addData("Stopped, color value is: ", String.format("%d", Values16[0]));
            telemetry.update();
            sleep(500);
            mtrFR.setPower(0.5);
            mtrFL.setPower(-0.5);
            mtrBR.setPower(0.5);
            mtrBL.setPower(-0.5);
            sleep(700);
            mtrFR.setPower(0.5);
            mtrFL.setPower(0.5);
            mtrBR.setPower(-0.5);
            mtrBL.setPower(-0.5);
            sleep(700);
            mtrFR.setPower(0);
            mtrFL.setPower(0);
            mtrBR.setPower(0);
            mtrBL.setPower(0);
    }
}
