package org.firstinspires.ftc.teamcode.test;

import android.app.Activity;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.AndroidGyro;

/**
 * Created by guinea on 10/1/17.
 * 1 is natural landscape camera
 * 3 is 180 degrees
 * 0 is 90 degrees cw
 */
@Disabled
@TeleOp(name="Gyro test", group="test")
public class AndroidGyroTest extends OpMode {
    AndroidGyro gyro;
    @Override
    public void init() {
        gyro = AndroidGyro.getInstance(hardwareMap.appContext);
    }

    @Override
    public void loop() {
        telemetry.addData("ScreenOrientation", Integer.toString(((Activity) hardwareMap.appContext).getWindowManager().getDefaultDisplay().getRotation()));
        if (gamepad1.a) gyro.reset();
        telemetry.addData("x", gyro.getX());
        telemetry.addData("y", gyro.getY());
        telemetry.addData("z", gyro.getZ());
        telemetry.addData("ax", gyro.ax);
        telemetry.addData("ay", gyro.ay);
        telemetry.addData("az", gyro.az);
        telemetry.update();
    }
}
