package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

    /**
     * Created by Sarahpambi76 on 11/26/17.
     */

    // 360*3=1,080
    // dispening servo is normal servo with limit of 1,080(3 rotations)
    // orientation servo is normal servo with limit of 360(1 rotation)
@Disabled
    @TeleOp (name = "Glyph Dispenser")

    public class GlyphDispenser extends OpMode {
        // Definitions
        Servo orientationSVO;
        Servo dispensingSVO;
        double rotLeft = 0/1080;
        double rotUp = 90/1080;
        double rotDown = 255/1080;
        double RotX = rotLeft;
        double orientX = 0;
        double orientY = 0.25;
        double orientB = 0.5;
        double orientA = 0.75;
        double orientStandard = orientY;
        double targetO = orientY;
        double targetD = rotLeft;



        @Override
        public void init(){

        }

        @Override
        //When init is pressed
        public void init_loop() {

            //HardwareMaps
            orientationSVO = hardwareMap.servo.get("orientationSVO");
            dispensingSVO = hardwareMap.servo.get("dispensingSVO");

            //Servos go to target positions
            dispensingSVO.setPosition(targetD);
            orientationSVO.setPosition(targetO);


        }

        //Start is pressed
        public void start() {

        }

        @Override
        public void loop() {


            //When the left part of dpad is pressed(Single Press), Dispensor is parallel ot the ground
            if (gamepad2.dpad_left) {RotX = rotLeft;}


            //When up on dpad is pressed on gamepad 2, dispensing servo is perpendicular to the ground
            if (gamepad2.dpad_up) {RotX = rotUp;}

            //When down on dpad is pressed on gamepad 2, dispensing servo is diagonal down
            if (gamepad2.dpad_down) {RotX = rotDown;}

            dispensingSVO.setPosition(RotX);


            //Single press of X on gamepad 2 results in servo orientating to 0
            if (gamepad2.x) {orientStandard = orientX;}

            //Single press of Y on gamepad 2 results in servo orientating to 90
            if (gamepad2.y) {orientStandard = orientY;}

            //Single press of B on gamepad 2 results in servo orientating to 180
            if (gamepad2.b) {orientStandard = orientB;}

            //Single press of A on gamepad 2 results in servo orientating to 270
            if (gamepad2.a) {orientStandard = orientA;}

            orientationSVO.setPosition(orientStandard);
            

        }


        public void stop() {

        }
    }

