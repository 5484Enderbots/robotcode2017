package org.firstinspires.ftc.teamcode.opmodes;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.teamcode.test.DriverCentricTeleop;
import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;
import org.firstinspires.ftc.teamcode.util.DriveBase;
import org.firstinspires.ftc.teamcode.util.Encoder;
import org.firstinspires.ftc.teamcode.util.Flipper;
import org.firstinspires.ftc.teamcode.util.PIDController;
import org.firstinspires.ftc.teamcode.vision.VuMarkReader;

import java.lang.annotation.Target;

/**
 * Created by Avery on 10/22/17.
 */
@Autonomous (name = "Near Stone Blue")
public class NearStoneBlue extends LinearOpMode {
    /**
     *
     * Constants for each servo position for the dispenser's orientation
     * degrees are clockwise
     */

    boolean fixFlipperDown = false;
    private static final double POS_DISPENSER_180 = 0.5254902; // aka OrientY
    private static final double POS_DISPENSER_90 = 0.28235294; // aka OrientX
    private static final double POS_DISPENSER_270 = 0.78823529; // aka OrientB
    private static final double POS_DISPENSER_0 = 0.01568627; // aka OrientA


    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */
    private static final double POS_RELEASE_1 = 0.88; // aka Release1
    private static final double POS_IDLE_1 = 0.23529412; // aka Idle1
    private static final double POS_GRABBED_1 = 0.03921569; // aka Grabbed1

    private static final double POS_RELEASE_2 = 1; // aka Release2
    private static final double POS_IDLE_2 = 0.325; // aka Idle2
    private static final double POS_GRABBED_2 = 0; // aka Grabbed2
    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */

    private static final double POS_JEWEL_UP = 0.7843;
    private static final double POS_JEWEL_DOWN = 0.1176;
    private static final double POS_COLOR_STRAIGHT = 21d/255d;
    private static final double POS_COLOR_RIGHT = 42d/255d;
    private static final double POS_COLOR_LEFT = 0d/255d;

    private static final double DIR_FORWARD = 0;
    private static final double DIR_BACKWARD = 180;
    private static final double DIR_LEFT = 270;
    private static final double DIR_RIGHT = 90;

    TouchSensor LowLimmit;
    TouchSensor HighLimmit;

    TouchSensor snsRelic;
    OpticalDistanceSensor snsGlyph1;
    OpticalDistanceSensor snsGlyphDispenser;
    Servo svoRelicWrist;
    Servo svoRelicClaw;
    Servo svoRelicArm;

    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorJewelR;
    ColorSensor16Bit snsColorJewelL;
    Encoder encFL;

    Servo svoStone;
    Servo svoJewel;
    Servo svoColor;
    Servo svoPhone;

    DcMotor mtrBL;
    DcMotor mtrBR;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrCollector;
    Servo svoGlyph1;
    Servo svoGlyph2;
    Servo svoCollect;
    Servo orientationSVO;
    //Servo dispensingSVO;
    DcMotor mtrDispense;
    ModernRoboticsI2cGyro snsGyro;
    TouchSensor snsProx1;
    TouchSensor snsProx2;

    Flipper flipper;
    BNO055IMU snsIMU;
    TouchSensor snsAllignD;
    TouchSensor snsAllignR;
    TouchSensor snsCollector;

    DriveBase driveBase;

    @Override
    public void runOpMode() {
        /*
        svoRelicArm = hardwareMap.servo.get("svoRelicArm");
        svoRelicArm.setPosition(RelicAngleBack);

        svoRelicClaw = hardwareMap.servo.get("svoRelicClaw");
        svoRelicClaw.setPosition(relicClawRelease);

        svoRelicWrist = hardwareMap.servo.get("svoRelicWrist");
        svoRelicWrist.setPosition(RelicWristFolded);
        */

        orientationSVO = hardwareMap.servo.get("orientationSVO");
        orientationSVO.setPosition(POS_DISPENSER_90);
        svoCollect = hardwareMap.servo.get("svoCollect");
        svoCollect.setPosition(0.5);

        svoJewel = hardwareMap.servo.get("svoJewel");
        svoColor = hardwareMap.servo.get("svoColor");



        svoJewel.setPosition(POS_JEWEL_UP);
        svoColor.setPosition(POS_COLOR_STRAIGHT);

        svoPhone = hardwareMap.servo.get("svoPhone");
        svoPhone.setPosition(0.72);

        snsAllignD = hardwareMap.touchSensor.get("snsAllignD");
        snsAllignR = hardwareMap.touchSensor.get("snsAllignR");

        snsProx1 = hardwareMap.touchSensor.get("snsProx1");
        snsProx2 = hardwareMap.touchSensor.get("snsProx2");

        svoGlyph1 = hardwareMap.servo.get("svoGlyph1");
        svoGlyph2 = hardwareMap.servo.get("svoGlyph2");
        svoGlyph1.setPosition(POS_RELEASE_1);
        svoGlyph2.setPosition(POS_RELEASE_2);

        LowLimmit = hardwareMap.touchSensor.get("LowLimmit");
        HighLimmit = hardwareMap.touchSensor.get("HighLimmit");

        mtrDispense = hardwareMap.dcMotor.get("mtrDispense");
        mtrDispense.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        // do the vuforia stuff
        RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.UNKNOWN;
        VuMarkReader vuMarkReader = new VuMarkReader(hardwareMap.appContext);
        vuMarkReader.start();

        mtrCollector = hardwareMap.dcMotor.get("mtrCollector");
        mtrCollector.setDirection(DcMotorSimple.Direction.REVERSE);
        snsCollector = hardwareMap.touchSensor.get("snsCollector");

        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");

        driveBase = new DriveBase(hardwareMap, false);

        svoStone = hardwareMap.servo.get("svoStone");
        svoStone.scaleRange(0, 1);
        svoStone.setPosition(0.17647);

        snsGlyph1 = hardwareMap.opticalDistanceSensor.get("snsGlyph1");
        snsGlyphDispenser = hardwareMap.opticalDistanceSensor.get("snsGlyphDispenser");

        encFL = new Encoder(mtrFL);

        mtrFL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrFR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");

        snsColorF = new ColorSensor16Bit(hardwareMap, "snsColorF", 0x3a);
        snsColorJewelR = new ColorSensor16Bit(hardwareMap, "snsColorJewelR", 0x38);
        snsColorJewelL = new ColorSensor16Bit(hardwareMap, "snsColorJewelL", 0x3e);

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled = true;
        parameters.loggingTag = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        snsIMU = hardwareMap.get(BNO055IMU.class, "snsIMU");
        snsIMU.initialize(parameters);

        snsGyro.calibrate();

        /*
            so, you see, the problem with waitForStart(); is that we need to continuously poll the
            vumark reader in a loop to get the vumark, right? the problem with that, however, is that
            waitForStart(); basically pauses the program until the play button is pressed. so we do a workaround:
        */

        telemetry.setAutoClear(false);
        telemetry.addLine("Initializing IMU...");
        telemetry.update();

        snsIMU = hardwareMap.get(BNO055IMU.class, "snsIMU");
        snsIMU.initialize(parameters);
        telemetry.setAutoClear(true);

        while (!isStarted() && !isStopRequested()) { // basically loops until the play button is pressed

            // only record a seen vumark if its actually useful:
            RelicRecoveryVuMark v = vuMarkReader.getVuMark();
            if (v != RelicRecoveryVuMark.UNKNOWN)
                vuMark = v;

            // give us some details on the status of the vumark and the gyro sensor
            telemetry.addData("Gyro calibrating: ", snsGyro.isCalibrating());
            telemetry.addData("VuMark", vuMark.toString());
            telemetry.update();
        }

        // shut down the vumark reader as the opmode starts
        vuMarkReader.stop();

        if (isStopRequested()) return;

        snsGyro.resetZAxisIntegrator();

        svoGlyph1.setPosition(POS_GRABBED_1);
        svoGlyph2.setPosition(POS_GRABBED_2);


        int currentHeading = Math.abs(snsGyro.getIntegratedZValue());
        senseJewel();
        /**
         * ROBOT DRIVES OFF OF BALANCING STONE, DRIVING FORWARD FOR BLUE, AND BACKWARDS FOR RED.
         * DRIVES USING ENCODER COUNTS, DRIVING 1500 COUNTS FOR BLUE AND 1350 COUNTS FOR RED.
         */
        driveEncoders(0.4, isBlueAlliance() ? DIR_FORWARD : DIR_BACKWARD, isBlueAlliance() ? 1500 : 1350);

        /**
         * ROBOT TURNS 90 DEGREES BASED ON THE GRYO HEADING
         * ROBOT TAKES INTO ACCOUNT DIFFERENCES FROM THE BALANCING STONE
         */

        turnTo(0.7, 270, 3, 0.0075, 0, 0.0005);
        DrivePower(0, 0, 0, 0);

        long ts = System.nanoTime();

        /**
         * ROBOT DRIVES UNTIL IT HITS THE CRYPTOBOX
         */
        while((!snsAllignR.isPressed() && encFL.getEncValue() > -1040 || !snsAllignD.isPressed() && encFL.getEncValue() > -1040) && opModeIsActive()){
            if (System.nanoTime() - ts > 2.5e9)
                break;
            DrivePower(0.4, -0.4, 0.4, -0.4);
        }
        DrivePower(0, 0, 0, 0);
        svoGlyph2.setPosition(POS_GRABBED_2);
        PlaceFirstGlyph(vuMark);
        /*
        PickSecondGlyph();
        PlaceGlyph(vuMark);
        */
        /**
         * ROBOT PLACES FIRST GLYPH, GOES BACK TO GLYPH PIT, AND PLACES MORE GLYPHS
         */


    }
    public void DrivePower(double fr, double fl, double br, double bl) {
        mtrBL.setPower(bl);
        mtrBR.setPower(br);
        mtrFL.setPower(fl);
        mtrFR.setPower(fr);
    }
    public void driveEncoders(double power, double direction, int distance) {
        /* basically drive in a direction for some set number of encoder counts
           this is a generalization of the commonly used construct
           the magic with driveBase.drive is that it computes the polarity of the motor powers
           based on a power and a direction - this allows it to turn say,
               DrivePower(-0.3, 0.3, -0.3, 0.3);
           into
               driveBase.drive(0.3, DIR_FORWARD);
           The directions themselves are just a measure in degrees, with 0 being forward, 90 being right,
           180 being backwards, and 270 being left. This is opposite the unit circle as positive magnitudes
           on all motors on this robot will spin it clockwise.
        */
        encFL.resetEncoder();
        while (Math.abs(encFL.getEncValue()) < distance && opModeIsActive())
            driveBase.drive(power, direction);
        driveBase.setDrivePowers(0, 0, 0, 0);
    }
    public void driveToColumn(double direction ) {
        /*
        This is a generalization of the common driving to column loop in glyph placement.
        It takes a direction, mostly.
         */
        encFL.resetEncoder();
        while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive())
            driveBase.drive(0.1, direction);
        driveBase.setDrivePowers(0, 0, 0, 0);
    }
    public void senseJewel() {
        svoJewel.setPosition(POS_JEWEL_DOWN);
        sleep(200); // decreased from 2000

        svoGlyph1.setPosition(POS_IDLE_1);
        svoGlyph2.setPosition(POS_GRABBED_2);
        sleep(800);

        int [] JewelColorsR = snsColorJewelR.ReadColor16();
        int blueJR = JewelColorsR [2];
        int RedJR = JewelColorsR [0];
        int ColorCompareR = blueJR - RedJR;

        int [] JewelColorsL = snsColorJewelL.ReadColor16();
        int blueJL = JewelColorsL [2];
        int RedJL = JewelColorsL [0];
        int ColorCompareL = blueJL - RedJL;

        //TODO: figure out how the sensors respond if a color sensor is detached or not reading anything

        boolean JewelFail1 = ColorCompareR == 0; // if the right sensor reads zero, we the right sensor isn't working.

        telemetry.clear();
        telemetry.setAutoClear(false);
        telemetry.addData("jewelFail", JewelFail1);
        telemetry.addData("ColorCompareR", ColorCompareR);
        telemetry.addData("ColorCompareL", ColorCompareL);


        // note that left and right are relative from the driver's point of view
        boolean hittingLeft;

        // R < 0 == red > blue
        if (!JewelFail1) // if the right color sensor works, use it for logic, else the left one.
            hittingLeft = (ColorCompareR < 0 && isBlueAlliance()) || (ColorCompareR > 0 && !isBlueAlliance());
        else
            hittingLeft = (ColorCompareL > 0 && isBlueAlliance()) || (ColorCompareL < 0 && !isBlueAlliance());

        telemetry.addData("hittingLeft", hittingLeft);
        telemetry.update();

        // hit the right jewel based on the conclusion we figured out above
        svoColor.setPosition(hittingLeft ? POS_COLOR_LEFT : POS_COLOR_RIGHT);
        sleep(300);
        svoJewel.setPosition(POS_JEWEL_UP);
        svoColor.setPosition(POS_COLOR_STRAIGHT);
        sleep(300);
        // negative = counterclockwise turning

    }
    public void PlaceFirstGlyph(RelicRecoveryVuMark vuMark) {
        telemetry.setAutoClear(false);
        /**
         * Robot backs up 30 encoder counts from the cryptobox
         */
        driveEncoders(0.35, DIR_FORWARD, 30);
        sleep(300);
        // Robot strafes to column, looking for reading on the proximity sensor or encoder counts as a backup
        // note that the directions are robot centric; after all, from the driver perspective, the robot is moving right although
        // it is strafing towards the left column
        if ((vuMark == RelicRecoveryVuMark.LEFT)) {// && isBlueAlliance()) || (vuMark == RelicRecoveryVuMark.RIGHT && !isBlueAlliance())) {
            // left code
            driveEncoders(0.1, DIR_RIGHT, 100);
            driveToColumn(DIR_RIGHT);
            driveToColumn(DIR_LEFT);

        } else if ((vuMark == RelicRecoveryVuMark.RIGHT)) {// && isBlueAlliance()) || (vuMark == RelicRecoveryVuMark.LEFT && !isBlueAlliance())) {
            // right code
            driveEncoders(0.1, DIR_LEFT, 200);
            driveToColumn(DIR_LEFT);
            driveToColumn(DIR_RIGHT);

        } else {
            // center code
            driveEncoders(0.1, DIR_RIGHT, 70);
            driveToColumn(DIR_LEFT);
            driveToColumn(DIR_RIGHT);
        }

        // raise the dispenser up
        mtrDispense.setTargetPosition(150);
        mtrDispense.setPower(0.5);
        svoCollect.setPosition(0);
        // drive a little away from the box, and release the glyph into the box.
        driveEncoders(0.35, DIR_FORWARD, 80);
        orientationSVO.setPosition(POS_DISPENSER_0);
        svoGlyph1.setPosition(POS_RELEASE_1);
        svoGlyph2.setPosition(POS_RELEASE_2);
        sleep(1000);
        svoCollect.setPosition(1);

        // push the glyph in and drive forward to avoid touching it.
        driveEncoders(0.1, DIR_FORWARD, 50);
        svoCollect.setPosition(0.5);
        driveEncoders(0.35, DIR_BACKWARD, 60);
        driveEncoders(0.2, DIR_FORWARD, 100);
    }
    public void PlaceGlyph(RelicRecoveryVuMark vuMark){
        /**
         * this method assumes that the column doesn't matter.
         * We'd use this for placing glyphs OTHER than the first glyph.
         * Used in situations where what column you put it in wont get any extra points.
         * Will use for multiglyph auto for additional glyphs.
         */
        snsGyro.resetZAxisIntegrator();
        mtrCollector.setPower(0);
        svoGlyph1.setPosition(POS_GRABBED_1);
        svoGlyph2.setPosition(POS_GRABBED_2);
        boolean GoLeft;
        if (vuMark == RelicRecoveryVuMark.CENTER || vuMark == RelicRecoveryVuMark.RIGHT){
            GoLeft = true;
        }
        else {
            GoLeft = false;
        }
        driveEncoders(0.2, GoLeft ? DIR_RIGHT : DIR_LEFT, 100);
        driveEncoders(0.3, DIR_BACKWARD, 100);

        svoGlyph1.setPosition(POS_RELEASE_1);
        svoGlyph2.setPosition(POS_RELEASE_2);
        sleep(1000);
        svoGlyph1.setPosition(POS_RELEASE_1);
        svoGlyph2.setPosition(POS_RELEASE_2);
        driveEncoders(0.1, DIR_FORWARD, 100);
        driveEncoders(1, DIR_BACKWARD, 200);
        driveEncoders(0.7, DIR_FORWARD, 50);
        svoGlyph2.setPosition(POS_IDLE_2);
        svoGlyph1.setPosition(POS_IDLE_1);
    }
    public void PickSecondGlyph() {
        snsGyro.resetZAxisIntegrator();

        /**
         * Flips dispenser down and robot moves forward
         */
        svoGlyph2.setPosition(POS_IDLE_2);
        svoGlyph1.setPosition(POS_IDLE_1);
        mtrDispense.setTargetPosition(-300);
        while (!LowLimmit.isPressed() && opModeIsActive()) {
            mtrDispense.setPower(-0.7);
            svoGlyph2.setPosition(POS_IDLE_2);
            svoGlyph1.setPosition(POS_IDLE_1);
        }
        mtrDispense.setPower(0);

        /**
         * Drives for 500 encoder counts to get to the glyph pit
         */
        snsGyro.resetZAxisIntegrator();
        driveEncoders(1, DIR_FORWARD, 800);


        /**
         * Resets gyro and prepares for the fll line follow to collect glyphs
         */

        encFL.resetEncoder();
        snsGyro.resetZAxisIntegrator();
        long ts = System.nanoTime();
        int x = 0;
        while (x < 2 && snsGlyphDispenser.getRawLightDetected() < 0.15 && opModeIsActive()) {
            mtrCollector.setPower(0.7);
            driveEncoders(0.6, DIR_FORWARD, 200);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            sleep(250);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            DrivePower(0, 0.3, 0, 0.3);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            sleep(700);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            DrivePower(0, -0.3, 0, -0.3);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                    break;
            }
            sleep(700);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            driveEncoders(0.3, DIR_BACKWARD, 200);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            mtrCollector.setPower(-0.5);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            sleep(200);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            mtrCollector.setPower(0.7);
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
            x = x + 1;
            if (snsGlyphDispenser.getRawLightDetected() > 0.15) {
                break;
            }
        }
            driveEncoders(0.6, DIR_BACKWARD, 500);
        turnTo(0.4, 270, 3, 0.0075, 0, 0.0005);
            svoGlyph2.setPosition(POS_GRABBED_2);
            svoGlyph1.setPosition(POS_GRABBED_1);
        mtrCollector.setPower(0);
        mtrDispense.setTargetPosition(500);
        while(!HighLimmit.isPressed() && opModeIsActive()){
            mtrDispense.setPower(1);
        }
        mtrDispense.setPower(0);
        mtrCollector.setPower(-0.5);
        sleep(200);
       driveEncoders(0.7, DIR_BACKWARD, 300);
        DrivePower(0, 0, 0, 0);
        mtrCollector.setPower(0);
    }
    public boolean isBlueAlliance() {
        return true;
    }
    public double calculateTurnError(double target) {
        // snsIMU
        // negated bc everything on our hecking robot is backwards reeeeee
        double angle = snsIMU.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle;
        telemetry.addData("angle", angle);
        double diff = (angle - target + 180) % 360 - 180;
        return diff < -180 ? diff + 360 : diff;
    }

    public void turnTo(double power_scale, double target, double tolerance, double Kp, double Ki, double Kd) {
        fixflipper_init();
        telemetry.setAutoClear(true);
        PIDController pid = new PIDController(Kp, Ki, Kd);
        double error = calculateTurnError(target);
        while (Math.abs(error) > tolerance && opModeIsActive()) {
            double m = Range.clip(pid.get(error) * power_scale, -1, 1);
            driveBase.setDrivePowers(m, m, m, m);
            error = calculateTurnError(target);
            telemetry.addData("turnError", error);
            telemetry.update();
            fixflipper_run();
        }
        driveBase.setDrivePowers(0, 0, 0, 0);

    }
    private void fixflipper_init() {
        if (fixFlipperDown && !LowLimmit.isPressed()) {
            svoGlyph1.setPosition(POS_IDLE_1);
            svoGlyph2.setPosition(POS_IDLE_2);
            mtrDispense.setTargetPosition(-300);
            mtrDispense.setPower(-0.7);
        }
    }
    private void fixflipper_run() {
        if (fixFlipperDown && LowLimmit.isPressed()) {
            mtrDispense.setPower(0);
            fixFlipperDown = false;
        }

    }
}

