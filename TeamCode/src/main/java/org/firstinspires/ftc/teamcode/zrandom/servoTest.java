package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;




/**
 * Created by Sarahpambi76 on 11/21/17.
 */

@TeleOp(name = "Servo Test", group = "MRI")  // @Autonomous(...) is the other common choice
@Disabled
public class servoTest extends LinearOpMode {


    private ElapsedTime runtime = new ElapsedTime();

    Servo servoCR;
    Servo servo;   //Create an instance of Servo
    double target = 0.5;   //create a variable for the target value. This will initially be 0.5.
    double targetCR = 0.5;
    boolean bButton, bButtonPressed, bLatchState;


    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        servo = hardwareMap.servo.get("left");   //Our servo will be called "left" in the configuration file.
        servoCR = hardwareMap.servo.get("CR");

        servo.setPosition(target);  //Initialize position
        // status();                   //Display the position and PWM mode
        servoCR.setPosition(targetCR);
        // status();

        waitForStart();

        while (opModeIsActive()) {

            /*while (!gamepad1.b) {

                servo.getController().pwmDisable();
                status();

                if (!gamepad1.b)
                    sleep(1000);

                servo.getController().pwmEnable();
                status();

                if (!gamepad1.b)
                    sleep(1000);
                telemetry.addData("Gamepad1.b", gamepad1.b);
                telemetry.update();

            } */

            telemetry.addData("Gamepad1.b", gamepad1.b);
            telemetry.update();

           /* if (gamepad1.b){
                bLatchState = true;
            }
            else {
                bLatchState = false;
            }

            */

           if (gamepad1.b) {

               if (!bButton) {
                   bLatchState = !bLatchState;
                   bButton = !bButton;
               } else {
               }
           }
           else {
                   bButton = false;
               }

           if (bLatchState) {


                float x = gamepad1.right_stick_x;
                //update target value proportionally using a joystick.
                target = 0.5 * x + 0.5;

                telemetry.addData("PositionX: ", target);
                telemetry.addData("GamePad 1 right stick", x);
                telemetry.update();

                servo.setPosition(target);  //Set position. This will also enable all six servo ports.

                float cr = gamepad2.right_stick_x;
                targetCR = 0.5 * cr + 0.5;

                telemetry.addData("PositionCR", targetCR);
                telemetry.addData("Gamepad 2 right stick", cr);
                telemetry.update();

                servoCR.setPosition(targetCR);
            }
            else {
                servo.getController().pwmDisable();
                servoCR.getController().pwmDisable();
            }

            sleep(40);

        }
    }
}




    /*void status() {
        if (servo.getController().getPwmStatus() == ServoController.PwmStatus.DISABLED) {
            telemetry.addData("PWM", "Disabled");
            telemetry.addData("Target", target);
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.update();
        } else {
            telemetry.addData("PWM", "Enabled");
            telemetry.addData("Target", target);
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.update();
        }
    }
} */