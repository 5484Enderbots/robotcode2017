package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;
import org.firstinspires.ftc.teamcode.util.DriveBase;

/**
 * Created by Avery on 9/11/17.
 */
@Disabled
@TeleOp(name="MeasureOp", group="K9bot")
public class MeasureOp extends OpMode {

    ColorSensor16Bit snsColorT;
    Servo svoStone;
    Servo svoJewel;
    DriveBase driveBase;
    DcMotor mtrCollector;
    Servo svoGlyph1;
    Servo svoGlyph2;
    Servo svoCollect;
    Servo orientationSVO;
    Servo dispensingSVO;
    DcMotor mtrExtend;
    DcMotor mtrAngle;
    double rotLeft = 0.08;
    double rotUp = .16;
    double rotDown = 0;
    double RotX = (21/255);
    double orientX = 0;
    double orientY = 0.15;
    double orientB = 0.5;
    double orientA = 0.75;
    double orientStandard = orientY;
    double targetO = orientY;
    //double targetD = rotLeft;

    public static final double MEDIUM_POWER = 3;
    public static final double LOW_POWER = 6;
    boolean xButton, xButtonPressed, xLatchState;

    public boolean Run_Collector = false;
    public boolean Stop_Collector = true;
    public boolean Slow_Collector = true;
    public boolean Fast_Collector = false;
    public boolean Reverse_Collector = false;

    public boolean Grabber_Release = true;
    public boolean Grabber_Idle = false;
    public boolean Grabber_Grabbed = false;
    public double Glyph_Release1 = 1;
    public double Glyph_Idle1 = 0.588;
    public double Glyph_Grab1 = 0;

    public double Glyph_Release2 = 0;
    public double Glyph_Idle2 = 0.784;
    public double Glyph_Grab2 = 1;

    public double CollectorPower;
    public double anglePower = 0.4;
    public double extendPower = 0.5;

    public ElapsedTime RUNTIME = new ElapsedTime();

    public void init_loop() {
        // pass, if we need it, it can be used later. both init and init_loop get called during opmode init.
    }
    @Override
    public void init(){

//        mtrFL = hardwareMap.dcMotor.get("mtrFL");
//        mtrFR = hardwareMap.dcMotor.get("mtrFR");
//        mtrBL = hardwareMap.dcMotor.get("mtrBL");
//        mtrBR = hardwareMap.dcMotor.get("mtrBR");
//        mtrFL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
//        mtrFR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
//        mtrBL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
//        mtrBR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        driveBase = new DriveBase(hardwareMap, false);

        //HardwareMaps
        orientationSVO = hardwareMap.servo.get("orientationSVO");
        dispensingSVO = hardwareMap.servo.get("dispensingSVO");

        //Servos go to target positions
        //dispensingSVO.setPosition(rotLeft);
       // dispensingSVO.setPosition(0);
      //  orientationSVO.setPosition(targetO);
        mtrCollector = hardwareMap.dcMotor.get("mtrCollector");
        svoGlyph1 = hardwareMap.servo.get("svoGlyph1");
        svoGlyph2 = hardwareMap.servo.get("svoGlyph2");
        svoJewel = hardwareMap.servo.get("svoJewel");
        svoCollect = hardwareMap.servo.get("svoCollect");
        svoStone = hardwareMap.servo.get("svoStone");
        mtrAngle = hardwareMap.dcMotor.get("mtrAngle");
        mtrExtend = hardwareMap.dcMotor.get("mtrExtend");
        svoStone.setPosition(0);
        snsColorT = new ColorSensor16Bit(hardwareMap, "snsColorT", 0x38);
        svoJewel.setPosition(0.7);

    }

    //Start is pressed
    public void start() {
        dispensingSVO.setPosition(rotUp);
        dispensingSVO.setPosition(rotLeft);
        orientationSVO.setPosition(0.1098);
        CollectorPower = -0.7;
        ElapsedTime RUNTIME = new ElapsedTime();
        svoStone.scaleRange(0, 1);
    }
    @Override
    public void loop(){

        int[] Values16T = snsColorT.ReadColor16();
        int blueT = Values16T[2];

        Values16T = snsColorT.ReadColor16();
        blueT = Values16T[2];
        telemetry.addData("jewel blue-ness", blueT);
        telemetry.update();

        if (gamepad1.y){
            svoCollect.setPosition(0);
        }
        else {
            svoCollect.setPosition(0.5);
        }

        if (gamepad2.dpad_right) {
            svoJewel.setPosition(0.7);
        }
        // Factor to divide the powers by
        double powerdiv = (1.7);
        //This controls the drive train
        double x1 = gamepad1.left_stick_x;
        double y1 = -gamepad1.left_stick_y;
        double x2 = gamepad1.right_stick_x;

        if (gamepad1.left_bumper) powerdiv = MEDIUM_POWER;
        if (gamepad1.left_trigger > 0.5) powerdiv = LOW_POWER;

        driveBase.driveArcade(x1, y1, x2, powerdiv);

        //When the left part of dpad is pressed(Single Press), Dispensor is parallel ot the ground
        if (gamepad2.dpad_left) {
           // RotX = rotLeft;
            dispensingSVO.setPosition(rotLeft);
            telemetry.addLine("dpad  left pressed");
        }


        //When up on dpad is pressed on gamepad 2, dispensing servo is perpendicular to the ground
        if (gamepad2.dpad_up) {
            RUNTIME.reset();
            while (RUNTIME.time() < 1.5){
                dispensingSVO.setPosition(0.2);
            }
            dispensingSVO.setPosition(rotUp);
            telemetry.addLine("dpad up pressed");
            //RotX = rotUp;
        }

        //When down on dpad is pressed on gamepad 2, dispensing servo is diagonal down
        if (gamepad2.dpad_down) {
           // RotX = rotDown;
            dispensingSVO.setPosition(rotDown);
            telemetry.addLine("dpad down pressed");
        }

        if (gamepad1.dpad_down){
            svoStone.setPosition(0.8);
        }
        else if (gamepad1.dpad_up){
            svoStone.setPosition(0.17647);
        }

        //dispensingSVO.setPosition(RotX);
        //telemetry.addData("ROTX", RotX);


        //Single press of X on gamepad 2 results in servo orientating to 0
        if (gamepad2.x) {
            //orientStandard = orientX;
        orientationSVO.setPosition(0.90196);
        }

        //Single press of Y on gamepad 2 results in servo orientating to 90
        if (gamepad2.y) {
            //orientStandard = orientY;}
            orientationSVO.setPosition(0.6275);}
        //Single press of B on gamepad 2 results in servo orientating to 180
        if (gamepad2.b) {
            //orientStandard = orientB;
            orientationSVO.setPosition(0.3529);}

        //Single press of A on gamepad 2 results in servo orientating to 270
        if (gamepad2.a) {
            //orientStandard = orientA;
            orientationSVO.setPosition(0.1098);}

       // orientationSVO.setPosition(orientStandard);

        if (gamepad2.left_trigger > 0.5){

            svoGlyph1.setPosition(0);
            svoGlyph2.setPosition(1);

        }
        else{
            svoGlyph1.setPosition(0.46);
            svoGlyph2.setPosition(0.86);
        }



        if (gamepad2.right_stick_y > 0.5){
            mtrExtend.setPower(extendPower);
        }
        else if (gamepad2.right_stick_y < -0.5){
            mtrExtend.setPower(-extendPower);
        }
        else{
            mtrExtend.setPower(0);
        }

        if (gamepad2.left_stick_y > 0.5){
            mtrAngle.setPower(anglePower);
        }
        else if (gamepad2.left_stick_y < -0.5){
            mtrAngle.setPower(-anglePower);
        }
        else{
            mtrAngle.setPower(0);
        }


        if (gamepad1.x){
            if (!xButton){
                xLatchState = !xLatchState;
                xButton = !xButton;
            } else {
            }
        }
        else {
            xButton = false;
        }


        if (xLatchState) {
            Run_Collector = true;
            Stop_Collector = false;
        }
        else {
            Run_Collector = false;
            Stop_Collector = true;
        }

        if (gamepad1.b){
            Fast_Collector = true;
            Slow_Collector = false;
            Reverse_Collector = false;
        }
        else if (gamepad1.a){
            Fast_Collector = false;
            Slow_Collector = true;
            Reverse_Collector = false;
        }


        if (gamepad1.right_trigger > 0.5){
            mtrCollector.setPower(0.5);
            Stop_Collector = false;
            Run_Collector = false;
        }
        else {

        }



        if (Fast_Collector){
            CollectorPower = -0.7;
        }
        else if (Slow_Collector){
            CollectorPower = -0.3;
        }
        if (Reverse_Collector){
            CollectorPower = 0.4;
        }


        if (Run_Collector){
            mtrCollector.setPower(CollectorPower);
        }
        else if (Stop_Collector) {
            mtrCollector.setPower(0);
        }






    }

    public void stop() {

    }

}
