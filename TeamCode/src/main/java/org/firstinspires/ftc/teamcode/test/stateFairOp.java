package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;


/**
 * Created by Sarahpambi76 on 8/6/18.
 */

@TeleOp(name="stateFairOp", group="K9bot")
public class stateFairOp extends OpMode{

    //Define sensors, motors, and variables.

    double Grabber_grab = 1;
    double Grabber_release = 0;

    DcMotor mtrFl;
    DcMotor mtrFr;
    DcMotor mtrBl;
    DcMotor mtrBr;
    DcMotor mtrAngle;
    DcMotor mtrExtend;

    Servo svoGrab;
    @Override
    public void init() {
        svoGrab = hardwareMap.servo.get("svoGrab");
        mtrAngle = hardwareMap.dcMotor.get("mtrAngle");
        mtrExtend = hardwareMap.dcMotor.get("mtrExtend");
        mtrBr = hardwareMap.dcMotor.get("mtrBr");
        mtrBl = hardwareMap.dcMotor.get("mtrBl");
        mtrFl = hardwareMap.dcMotor.get("mtrFl");
        mtrFr = hardwareMap.dcMotor.get("mtrFr");
    }
    @Override
    public void loop() {

        double x = gamepad1.left_stick_y;
        double y = gamepad1.right_stick_y;
        mtrFl.setPower(x);
        mtrBl.setPower(x);
        mtrFr.setPower(y);
        mtrBr.setPower(y);

        mtrExtend.setPower(gamepad2.right_stick_y);
        mtrAngle.setPower(gamepad2.left_stick_y);

        if(gamepad2.a){
            svoGrab.setPosition(Grabber_grab);
        }
        else if(gamepad2.b){
            svoGrab.setPosition(Grabber_release);
        }
    }
}
