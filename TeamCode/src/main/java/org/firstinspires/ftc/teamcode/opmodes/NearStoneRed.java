package org.firstinspires.ftc.teamcode.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

/**
 * Created by Avery on 3/16/18.
 */


@Autonomous(name = "NearStoneRed")
public class NearStoneRed extends NearStoneBlue {

    public boolean isBlueAlliance() {
        return false;
    }
}