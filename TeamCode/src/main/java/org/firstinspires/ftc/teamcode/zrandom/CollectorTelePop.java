package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.util.DriveBase;

/**
 * Created by Sarahpambi76 on 11/26/17.
 */

@Disabled
@TeleOp (name = "Collector TelePOp")

public class CollectorTelePop extends OpMode {
    DriveBase driveBase;
    Servo orientationServo;
    Servo dispensingServo;
    double targetO = 0.5;
    double targetD = 0;


    @Override
    public void init() {

        dispensingServo.setPosition(targetD);
        orientationServo.setPosition(targetO);


    }

    @Override
    public void init_loop() {

        driveBase = new DriveBase(hardwareMap, false);

    }

    public void start() {

    }

    @Override
    public void loop() {

        if (gamepad2.dpad_left) {



        }
        else {

        }

        if (gamepad2.dpad_up) {


        }
    }

    public void stop() {

    }
}