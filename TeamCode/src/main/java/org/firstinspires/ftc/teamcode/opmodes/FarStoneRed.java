package org.firstinspires.ftc.teamcode.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

/**
 * Created by guinea on 3/9/18.
 */

@Autonomous(name = "Far Stone Red")
public class FarStoneRed extends FarStoneBlue {
    @Override
    public boolean isBlueAlliance() {
        return false;
    }
}
