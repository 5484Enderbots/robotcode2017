package org.firstinspires.ftc.teamcode.test;

import android.text.method.Touch;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.TouchSensor;

import org.firstinspires.ftc.teamcode.util.Encoder;

/**
 * Created by Avery on 12/11/17.
 */
@Disabled
@TeleOp (name = "RunToPositionTest", group="K9bot")
public class RunToPositionTest extends OpMode {

    DcMotor mtrDispense;
    TouchSensor LowLimmit;
    //TouchSensor HighLimmit;
    public boolean LowStop = false;
    //public boolean HighStop = false;
    public boolean DispenserMoving = false;

    @Override
    public void init(){

        mtrDispense = hardwareMap.dcMotor.get("mtrDispense");
        mtrDispense.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        LowLimmit = hardwareMap.touchSensor.get("LowLimmit");
        //HighLimmit = hardwareMap.touchSensor.get("HighLimmit");
    }
    @Override
    public void loop(){

        if (gamepad1.dpad_left){
            mtrDispense.setTargetPosition(1120);
            mtrDispense.setPower(1);
        }

        if (gamepad1.dpad_down && !LowStop){
            mtrDispense.setTargetPosition(0);
            mtrDispense.setPower(1);
        }
        if (LowStop){mtrDispense.setPower(0);}
        //if (HighLimmit.isPressed()){HighStop = true;} else{HighStop = false;}
        if (LowLimmit.isPressed()){LowStop = true;} else{LowStop = false;}

    }

}
