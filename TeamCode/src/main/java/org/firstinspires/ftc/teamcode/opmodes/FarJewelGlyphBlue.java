package org.firstinspires.ftc.teamcode.opmodes;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;
import org.firstinspires.ftc.teamcode.util.Encoder;
import org.firstinspires.ftc.teamcode.util.Flipper;
import org.firstinspires.ftc.teamcode.vision.VuMarkReader;

/**
 * Created by Avery on 10/22/17.
 */
@Disabled
@Autonomous (name = "FarJewelGlyphBlue")
public class FarJewelGlyphBlue extends LinearOpMode {
    TouchSensor LowLimmit;
    TouchSensor HighLimmit;
    public boolean LowStop = false;
    public boolean HighStop = false;
    public boolean DispenserMoving = false;

    TouchSensor snsRelic;
    OpticalDistanceSensor snsGlyph1;
    OpticalDistanceSensor snsGlyphDispenser;
    Servo svoRelicWrist;
    Servo svoRelicClaw;
    Servo svoRelicArm;

    ColorSensor16Bit snsColorF;
    ColorSensor16Bit snsColorJewelR;
    ColorSensor16Bit snsColorJewelL;
    Encoder encFL;
    Servo svoStone;
    Servo svoJewel;
    DcMotor mtrBL;
    DcMotor mtrBR;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrCollector;
    Servo svoGlyph1;
    Servo svoGlyph2;
    Servo svoCollect;
    Servo orientationSVO;
    //Servo dispensingSVO;
    DcMotor mtrDispense;
    DcMotor mtrRelicExtend;
    ModernRoboticsI2cGyro snsGyro;
    TouchSensor snsProx1;
    TouchSensor snsProx2;

    Flipper flipper;

    //DcMotor mtrAngle;
    int rotLeft = 1850;
    int rotUp = 0;
    int rotDown = 3000;
    double rotPower = 0.6;


    TouchSensor snsAllignD;
    TouchSensor snsAllignR;
    TouchSensor snsCollector;

    /**
     *
     * Constants for each servo position for the Relic Grabber, wrist, claw, and encoder positions for slide
     *
     */
    public double relicClawGrab = 0.82352941;
    public double relicClawRelease = 0.17647059;

    public double RelicAngleDown = 0.37647059;
    public double RelicAngleBack = 0.54901961;
    public double RelicAngleUp = 0.45098039;

    public double RelicWristFolded = 0.19607843;
    public double RelicWristOut = 0.07843137;

    public int RelicSlideIn = 0;
    public int RelicSlideMid = 4000;
    public int RelicSlideOut = 7950;

    /**
     *
     * Constants for each servo position for the Relic Grabber, wrist, claw, and encoder positions for slide
     *
     */



    /**
     *
     * Constants for each servo position for the dispenser's orientation
     *
     */
    public double OrientY = 0.5254902;
    public double OrientX = 0.28235294;
    public double OrientB = 0.78823529;
    public double OrientA = 0.01568627;
    /**
     *
     * Constants for each servo position for the dispenser's orientation
     *
     */




    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */
    public double Release1 = 0.88;
    public double Idle1 = 0.23529412;
    public double Grabbed1 = 0.03921569;

    public double Release2 = 1;
    public double Idle2 = 0.25;
    public double Grabbed2 = 0;
    /**
     *
     * Constants for each servo position for the Glyph Grabbers
     *
     */




    public static final double LOW_POWER = 10;
    public static final double FAST_POWER = 1;
    public static final double NORMAL_POWER = 1.5;
    boolean xButton, xButtonPressed, xLatchState;

    public boolean Run_Collector = false;
    public boolean Stop_Collector = true;
    public boolean Slow_Collector = true;
    public boolean Fast_Collector = false;
    public boolean Reverse_Collector = false;

    boolean dispenserRaising = false;

    public double CollectorPower;
    public double anglePower = 0.4;
    public double extendPower = 0.67;
    public double retractPower = 0.4;

    ElapsedTime dispenserRuntime = new ElapsedTime();

    @Override
    public void runOpMode() {

        svoRelicArm = hardwareMap.servo.get("svoRelicArm");
        svoRelicArm.setPosition(RelicAngleBack);

        svoRelicClaw = hardwareMap.servo.get("svoRelicClaw");
        svoRelicClaw.setPosition(relicClawRelease);

        svoRelicWrist = hardwareMap.servo.get("svoRelicWrist");
        svoRelicWrist.setPosition(RelicWristFolded);

        orientationSVO = hardwareMap.servo.get("orientationSVO");
        orientationSVO.setPosition(OrientX);
        svoCollect = hardwareMap.servo.get("svoCollect");
        svoCollect.setPosition(0.5);
        svoJewel = hardwareMap.servo.get("svoJewel");
        svoJewel.setPosition(0.9);

        snsAllignD = hardwareMap.touchSensor.get("snsAllignD");
        snsAllignR = hardwareMap.touchSensor.get("snsAllignR");

        snsProx1 = hardwareMap.touchSensor.get("snsProx1");
        snsProx2 = hardwareMap.touchSensor.get("snsProx2");

        svoGlyph1 = hardwareMap.servo.get("svoGlyph1");
        svoGlyph2 = hardwareMap.servo.get("svoGlyph2");
        svoGlyph1.setPosition(Release1);
        svoGlyph2.setPosition(Release2);

        LowLimmit = hardwareMap.touchSensor.get("LowLimmit");

        mtrDispense = hardwareMap.dcMotor.get("mtrDispense");
        mtrDispense.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        // do the vuforia stuff
        RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.UNKNOWN;
        VuMarkReader vuMarkReader = new VuMarkReader(hardwareMap.appContext);
        vuMarkReader.start();

        mtrCollector = hardwareMap.dcMotor.get("mtrCollector");
        snsCollector = hardwareMap.touchSensor.get("snsCollector");

        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");

        svoStone = hardwareMap.servo.get("svoStone");
        svoStone.scaleRange(0, 1);
        svoStone.setPosition(0.17647);

        snsGlyph1 = hardwareMap.opticalDistanceSensor.get("snsGlyph1");
        snsGlyphDispenser = hardwareMap.opticalDistanceSensor.get("snsGlyphDispenser");

        encFL = new Encoder(mtrFL);

        mtrFL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrFR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");

        snsColorF = new ColorSensor16Bit(hardwareMap, "snsColorF", 0x3a);

        snsColorJewelR = new ColorSensor16Bit(hardwareMap, "snsColorJewelR", 0x38);
        snsColorJewelL = new ColorSensor16Bit(hardwareMap, "snsColorJewelL", 0x3e);
        snsGyro.resetZAxisIntegrator();

        snsGyro.calibrate();

        /*
            so, you see, the problem with waitForStart(); is that we need to continuously poll the
            vumark reader in a loop to get the vumark, right? the problem with that, however, is that
            waitForStart(); basically pauses the program until the play button is pressed. so we do a workaround:
        */

        while (!isStarted() && !isStopRequested()) { // basically loops until the play button is pressed

            // only record a seen vumark if its actually useful:
            RelicRecoveryVuMark v = vuMarkReader.getVuMark();
            if (v != RelicRecoveryVuMark.UNKNOWN)
                vuMark = v;

            // give us some details on the status of the vumark and the gyro sensor
            telemetry.addData("Gyro calibrating: ", snsGyro.isCalibrating());
            telemetry.addData("VuMark", vuMark.toString());
            telemetry.update();
        }

        // shut down the vumark reader as the opmode starts
        vuMarkReader.stop();

        if (isStopRequested()) return;

        snsGyro.resetZAxisIntegrator();

        svoGlyph1.setPosition(Idle1);
        svoGlyph2.setPosition(Grabbed2);

        snsGyro.resetZAxisIntegrator();
        int currentHeading = snsGyro.getIntegratedZValue();
        SenseJewel();
        encFL.resetEncoder();
        while(Math.abs(encFL.getEncValue()) < (isBlueAlliance() ? 1200 : 1200) && opModeIsActive()) {
            if (isBlueAlliance())
                DrivePower(-0.3, 0.3, -0.3, 0.3);
            else
                DrivePower(0.3, -0.3, 0.3, -0.3);
        }
        DrivePower(0, 0, 0, 0);
        encFL.resetEncoder();
        while(Math.abs(encFL.getEncValue()) < (isBlueAlliance() ? 200 : 200) && opModeIsActive()) {
            if (isBlueAlliance())
                DrivePower(0.3, -0.3, 0.3, -0.3);
            else
                DrivePower(-0.3, 0.3, -0.3, 0.3);
        }
        DrivePower(0, 0, 0, 0);
        currentHeading = snsGyro.getIntegratedZValue();
        int targetHeading = 79;

        while (currentHeading < targetHeading && opModeIsActive()){
            DrivePower(0.3, 0.3, 0.3, 0.3);
            currentHeading = Math.abs(snsGyro.getIntegratedZValue());
        }
        DrivePower(0, 0, 0, 0);
        encFL.resetEncoder();
        // isBlueAlliance() ? -1120 : -1000
        long ts = System.nanoTime();
        while((!snsAllignR.isPressed() && encFL.getEncValue() > (-1040) || !snsAllignD.isPressed() && encFL.getEncValue() > (-1040)) && opModeIsActive()){
            if (System.nanoTime() - ts > 2.5e9)
                break;
            DrivePower(0.3, -0.3, 0.3, -0.3);
        }
        DrivePower(0, 0, 0, 0);
        snsGyro.resetZAxisIntegrator();
        encFL.resetEncoder();
        while(Math.abs(encFL.getEncValue()) < (1120)){
            DrivePower(-0.3, 0.3, -0.3, 0.3);
        }
        DrivePower(0, 0, 0, 0);
        currentHeading = Math.abs(snsGyro.getIntegratedZValue());
        targetHeading = 79;

        while (currentHeading < targetHeading && opModeIsActive()){
            if (isBlueAlliance()) {
                DrivePower(0.3, 0.3, 0.3, 0.3);
                currentHeading = Math.abs(snsGyro.getIntegratedZValue());
            }
            else{
                DrivePower(-0.3, -0.3, -0.3, -0.3);
                currentHeading = Math.abs(snsGyro.getIntegratedZValue());
            }
        }
        DrivePower(0, 0, 0, 0);
        encFL.resetEncoder();
        ts = System.nanoTime();
        while((!snsAllignR.isPressed() && encFL.getEncValue() > (-1040) || !snsAllignD.isPressed() && encFL.getEncValue() > (-1040)) && opModeIsActive()){
            if (System.nanoTime() - ts > 2.5e9)
                break;
            DrivePower(0.3, -0.3, 0.3, -0.3);
        }
        DrivePower(0, 0, 0, 0);

        svoGlyph2.setPosition(Grabbed2);
        PlaceFirstGlyph(vuMark);
        //PickSecondGlyph();
        //PlaceGlyph();
        stop();
    }

    public void DrivePower(double fr, double fl, double br, double bl) {
        mtrBL.setPower(bl);
        mtrBR.setPower(br);
        mtrFL.setPower(fl);
        mtrFR.setPower(fr);
    }
    public void SenseJewel() {
        svoJewel.setPosition(0.15686275);
        sleep(2000);
        int [] JewelColorsR = snsColorJewelR.ReadColor16();
        int blueJR = JewelColorsR [2];
        int RedJR = JewelColorsR [0];
        int ColorCompareR = blueJR - RedJR;

        int [] JewelColorsL = snsColorJewelL.ReadColor16();
        int blueJL = JewelColorsL [2];
        int RedJL = JewelColorsL [0];
        int ColorCompareL = blueJL - RedJL;

        boolean JewelFail1 = false;
        boolean JewelFail2 = false;

        if ((ColorCompareR < 0 && isBlueAlliance()) || (ColorCompareR > 0 && !isBlueAlliance())) {
            JewelFail1 = false;
            encFL.resetEncoder();
            while (encFL.getEncValue() > -90&& opModeIsActive()) {
                DrivePower(-0.1, -0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            svoJewel.setPosition(1);
            encFL.resetEncoder();
            while (encFL.getEncValue() < 90&& opModeIsActive()) {
                DrivePower(0.1, 0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            if (JewelFail1){
                if ((ColorCompareL > 0 && isBlueAlliance()) || (ColorCompareL < 0 && !isBlueAlliance())) {
                    JewelFail2 = false;
                    encFL.resetEncoder();
                    while (encFL.getEncValue() > -90&& opModeIsActive()) {
                        DrivePower(-0.1, -0.1, -0.1, -0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                    svoJewel.setPosition(1);
                    encFL.resetEncoder();
                    while (encFL.getEncValue() < 90&& opModeIsActive()) {
                        DrivePower(0.1, 0.1, 0.1, 0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                } else if ((ColorCompareL < 0 && isBlueAlliance()) || (ColorCompareL > 0 && !isBlueAlliance())){
                    JewelFail2 = false;
                    encFL.resetEncoder();
                    while (encFL.getEncValue() < 90&& opModeIsActive()) {
                        DrivePower(0.1, 0.1, 0.1, 0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                    svoJewel.setPosition(1);
                    encFL.resetEncoder();
                    while (encFL.getEncValue() > -90&& opModeIsActive()) {
                        DrivePower(-0.1, -0.1, -0.1, -0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                }
                else{
                    JewelFail2 = true;
                    svoJewel.setPosition(1);
                }
            }
        } else if ((ColorCompareR > 0 && isBlueAlliance()) || (ColorCompareR < 0 && !isBlueAlliance())){
            JewelFail1 = false;
            encFL.resetEncoder();
            while (encFL.getEncValue() < 90&& opModeIsActive()) {
                DrivePower(0.1, 0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            svoJewel.setPosition(1);
            encFL.resetEncoder();
            while (encFL.getEncValue() > -90&& opModeIsActive()) {
                DrivePower(-0.1, -0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            if (JewelFail1){
                if ((ColorCompareL > 0 && isBlueAlliance()) || (ColorCompareL < 0 && !isBlueAlliance())) {
                    JewelFail2 = false;
                    encFL.resetEncoder();
                    while (encFL.getEncValue() > -90&& opModeIsActive()) {
                        DrivePower(-0.1, -0.1, -0.1, -0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                    svoJewel.setPosition(1);
                    encFL.resetEncoder();
                    while (encFL.getEncValue() < 90&& opModeIsActive()) {
                        DrivePower(0.1, 0.1, 0.1, 0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                } else if ((ColorCompareL < 0 && isBlueAlliance()) || (ColorCompareL > 0 && !isBlueAlliance())){
                    JewelFail2 = false;
                    encFL.resetEncoder();
                    while (encFL.getEncValue() < 90&& opModeIsActive()) {
                        DrivePower(0.1, 0.1, 0.1, 0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                    svoJewel.setPosition(1);
                    encFL.resetEncoder();
                    while (encFL.getEncValue() > -90&& opModeIsActive()) {
                        DrivePower(-0.1, -0.1, -0.1, -0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                }
                else{
                    JewelFail2 = true;
                    svoJewel.setPosition(1);
                }
            }
        } else{
            JewelFail1 = true;
            if (JewelFail1){
                if ((ColorCompareL > 0 && isBlueAlliance()) || (ColorCompareL < 0 && !isBlueAlliance())) {
                    JewelFail2 = false;
                    encFL.resetEncoder();
                    while (encFL.getEncValue() > -90&& opModeIsActive()) {
                        DrivePower(-0.1, -0.1, -0.1, -0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                    svoJewel.setPosition(1);
                    encFL.resetEncoder();
                    while (encFL.getEncValue() < 90&& opModeIsActive()) {
                        DrivePower(0.1, 0.1, 0.1, 0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                } else if ((ColorCompareL < 0 && isBlueAlliance()) || (ColorCompareL > 0 && !isBlueAlliance())){
                    JewelFail2 = false;
                    encFL.resetEncoder();
                    while (encFL.getEncValue() < 90&& opModeIsActive()) {
                        DrivePower(0.1, 0.1, 0.1, 0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                    svoJewel.setPosition(1);
                    encFL.resetEncoder();
                    while (encFL.getEncValue() > -90&& opModeIsActive()) {
                        DrivePower(-0.1, -0.1, -0.1, -0.1);
                    }
                    DrivePower(0, 0, 0, 0);
                }
                else{
                    JewelFail2 = true;
                    svoJewel.setPosition(1);
                }
            }
        }

    }
    public void PickSecondGlyph() {
        /**
         * Prepares for picking next glyph by flipping out collector and moving forward to avoid hitting cryptobox
         */
        svoGlyph2.setPosition(Idle2);
        encFL.resetEncoder();
        while (encFL.getEncValue() < 500) {
            DrivePower(-0.4, 0.4, -0.4, 0.4);
        }
        DrivePower(0, 0, 0, 0);
        mtrDispense.setTargetPosition(1850);
        orientationSVO.setPosition(OrientA);
        mtrDispense.setPower(0.6);
        svoCollect.setPosition(0);
        sleep(2500);
        svoCollect.setPosition(1);
        mtrDispense.setTargetPosition(-1120);
        mtrDispense.setPower(0.7);
        sleep(1500);
        svoCollect.setPosition(0.5);

        /**
         * Drives toward the center of the glyph pit a little before the dance to grab glyphs
         */
        encFL.resetEncoder();
        DrivePower(-0.36, 0.36, -0.36, 0.36);
        mtrCollector.setPower(0.5);
        sleep(800);
        DrivePower(0, 0, 0, 0);
        sleep(200);
        /**
         * reset's gyro, and does a tank turn on opposite sides until the touch sensor in the collector picks up a glyph
         */
        snsGyro.resetZAxisIntegrator();
        long ts = System.nanoTime();
        while (snsCollector.isPressed() && opModeIsActive()) {
            DrivePower(-0.3, 0, -0.3, 0);
            sleep(500);
            DrivePower(0, 0.3, 0, 0.3);
            sleep(500);
            if (System.nanoTime() - ts > 4e9)
                break;
        }
        mtrCollector.setPower(0);
        /**
         * takes how much the robot is off by from the turn's and adjusts to get back to 0 so we're straightened out
         */
        int currentHeading = snsGyro.getIntegratedZValue();
        int targetHeading = 0;

        while (currentHeading > targetHeading && opModeIsActive()) {
            DrivePower(-0.2, -0.2, -0.2, -0.2);
            currentHeading = Math.abs(snsGyro.getIntegratedZValue());
        }
        DrivePower(0, 0, 0, 0);
        /**
         * backs away from glyph pit, and jogs the collector to account for 45 degree angle glyphs
         */
        encFL.resetEncoder();
        while (Math.abs(encFL.getEncValue()) < 500 && opModeIsActive()){
            DrivePower(-0.36, 0.36, -0.36, 0.36);
        }
        DrivePower(0, 0, 0, 0);
        mtrCollector.setPower(0.5);
        sleep(500);
        mtrCollector.setPower(-0.5);
        sleep(500);
        mtrCollector.setPower(0.5);
        sleep(500);
        mtrCollector.setPower(-0.6);
        sleep(200);
        mtrCollector.setPower(0.7);
        sleep(1000);
        DrivePower(0.4, -0.4, 0.4, -0.4);
        sleep(400);
        mtrCollector.setPower(0);
        DrivePower(0, 0, 0, 0);
        /**
         * grabs glyphs and heads back to cryptobox for placing the glyph
         */
        svoGlyph1.setPosition(Grabbed1);
        svoGlyph2.setPosition(Grabbed2);
        mtrDispense.setTargetPosition(0);
        mtrDispense.setPower(0.4);
        encFL.resetEncoder();
        while (encFL.getEncValue() > (-120) && opModeIsActive()) {
            DrivePower(-0.2, -0.2, 0.2, 0.2);
        }
        DrivePower(0, 0, 0, 0);
        mtrCollector.setPower(0);
        ts = System.nanoTime();
        encFL.resetEncoder();
        mtrCollector.setPower(-0.5);
        while((!snsAllignR.isPressed()  || !snsAllignD.isPressed()) && opModeIsActive()){
            if (System.nanoTime() - ts > 2.5e9) // 2.5 seconds
                break;
            DrivePower(0.4, -0.4, 0.4, -0.4);
        }
        DrivePower(0, 0, 0, 0);
        mtrCollector.setPower(0);
    }
    public void PlaceFirstGlyph(RelicRecoveryVuMark vuMark) {
        telemetry.setAutoClear(false);
        if ((vuMark == RelicRecoveryVuMark.LEFT)) {// && isBlueAlliance()) || (vuMark == RelicRecoveryVuMark.RIGHT && !isBlueAlliance())) {
            // left code

            /**
             * lets the driver know which column it's going for
             */
            telemetry.addLine("LEFT SIDE BOYS");
            telemetry.update();

            /**
             * Robot backs up 30 encoder counts from the cryptobox
             */
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
            }
            DrivePower(0, 0, 0, 0);
            sleep(300);
            /**
             * Robot strafes to column, left in this case, looking for reading on the proximity sensor or encoder counts as a backup
             */
            encFL.resetEncoder();
            while (Math.abs(encFL.getEncValue()) < (100) && opModeIsActive()){
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (80) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
                mtrDispense.setTargetPosition(1000);
                mtrDispense.setPower(0.5);
            }
            DrivePower(0, 0, 0, 0);
            orientationSVO.setPosition(OrientA);
            sleep(1000);
            svoGlyph1.setPosition(Release1);
            svoGlyph2.setPosition(Release2);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (50) && opModeIsActive()) {
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0 ,0);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-60) && opModeIsActive()){
                DrivePower( 0.35, -0.35, 0.35, -0.35);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while(encFL.getEncValue() < (100)){
                DrivePower(-0.2, 0.2, -0.2, 0.2);
            }
            DrivePower(0, 0, 0, 0);


        } else if ((vuMark == RelicRecoveryVuMark.RIGHT)) {// && isBlueAlliance()) || (vuMark == RelicRecoveryVuMark.LEFT && !isBlueAlliance())) {
            // right code


            /**
             * lets the driver know which column it's going for
             */
            telemetry.addLine("RIGHT SIDE BOYS");
            telemetry.update();

            /**
             * Robot backs up 30 encoder counts from the cryptobox
             */
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
            }
            DrivePower(0, 0, 0, 0);
            sleep(300);
            /**
             * Robot strafes to column, left in this case, looking for reading on the proximity sensor or encoder counts as a backup
             */
            encFL.resetEncoder();
            while (Math.abs(encFL.getEncValue()) < (200) && opModeIsActive()){
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (80) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
                mtrDispense.setTargetPosition(1000);
                mtrDispense.setPower(0.5);
            }
            DrivePower(0, 0, 0, 0);
            orientationSVO.setPosition(OrientA);
            sleep(1000);
            svoGlyph1.setPosition(Release1);
            svoGlyph2.setPosition(Release2);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (50) && opModeIsActive()) {
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0 ,0);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-60) && opModeIsActive()){
                DrivePower( 0.35, -0.35, 0.35, -0.35);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while(encFL.getEncValue() < (100)){
                DrivePower(-0.2, 0.2, -0.2, 0.2);
            }
            DrivePower(0, 0, 0, 0);
        } else {
            // center code


            /**
             * lets the driver know which column it's going for
             */
            telemetry.addLine("CENTER BOYS");
            telemetry.update();

            /**
             * Robot backs up 30 encoder counts from the cryptobox
             */
            encFL.resetEncoder();
            while (encFL.getEncValue() < (30) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
            }
            DrivePower(0, 0, 0, 0);
            sleep(300);
            /**
             * Robot strafes to column, left in this case, looking for reading on the proximity sensor or encoder counts as a backup
             */
            encFL.resetEncoder();
            while (Math.abs(encFL.getEncValue()) < (70) && opModeIsActive()){
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            while (snsProx2.isPressed() && snsProx1.isPressed() && opModeIsActive()) {
                DrivePower(0.1, 0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (80) && opModeIsActive()) {
                DrivePower(-0.35, 0.35, -0.35, 0.35);
                mtrDispense.setTargetPosition(1000);
                mtrDispense.setPower(0.5);
            }
            DrivePower(0, 0, 0, 0);
            orientationSVO.setPosition(OrientA);
            sleep(1000);
            svoGlyph1.setPosition(Release1);
            svoGlyph2.setPosition(Release2);
            encFL.resetEncoder();
            while (encFL.getEncValue() < (50) && opModeIsActive()) {
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0 ,0);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-60) && opModeIsActive()){
                DrivePower( 0.35, -0.35, 0.35, -0.35);
            }
            DrivePower(0, 0, 0, 0);
            encFL.resetEncoder();
            while(encFL.getEncValue() < (100)){
                DrivePower(-0.2, 0.2, -0.2, 0.2);
            }
            DrivePower(0, 0, 0, 0);
        }
    }
    public void PlaceGlyph(){
        /**
         * this method assumes that the column doesn't matter.
         * We'd use this for placing glyphs OTHER than the first glyph.
         * Used in situations where what column you put it in wont get any extra points.
         * Will use for multiglyph auto for additional glyphs.
         */
        encFL.resetEncoder();
        while (encFL.getEncValue() < (500) && opModeIsActive()) {
            DrivePower(-0.35, 0.35, -0.35, 0.35);
        }
        DrivePower(0, 0, 0, 0);

        mtrDispense.setTargetPosition(1850);
        mtrDispense.setPower(0.4);
        sleep(300);
        encFL.resetEncoder();
        while (encFL.getEncValue() > (-80) && opModeIsActive()) {
            DrivePower(-0.3, -0.3, 0.3, 0.3);
        }
        DrivePower(0, 0, 0, 0);
        orientationSVO.setPosition(OrientA);
        sleep(700);

        encFL.resetEncoder();
        while (encFL.getEncValue() > (-230) && opModeIsActive()) {
            DrivePower(0.1, -0.1, 0.1, -0.1);
        }
        DrivePower(0, 0, 0, 0);
        svoGlyph1.setPosition(Release1);
        svoGlyph2.setPosition(Release2);

        sleep(1000);
        DrivePower(0.6, -0.6, 0.6, -0.6);
        sleep(1500);
        DrivePower(0, 0, 0, 0);

        encFL.resetEncoder();
        while (encFL.getEncValue() < 200 && opModeIsActive()) {
            DrivePower(-0.3, 0.3, -0.3, 0.3);
        }
        DrivePower(0, 0, 0, 0);


        mtrDispense.setTargetPosition(1850);
        mtrDispense.setPower(0.4);


    }
    public boolean isBlueAlliance() {
        return true;
    }
}

