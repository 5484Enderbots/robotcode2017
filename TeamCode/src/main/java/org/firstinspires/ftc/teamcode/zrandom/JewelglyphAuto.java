package org.firstinspires.ftc.teamcode.zrandom;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cColorSensor;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.I2cAddr;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.teamcode.util.ColorSensor16Bit;
import org.firstinspires.ftc.teamcode.util.Encoder;
import org.firstinspires.ftc.teamcode.vision.VuMarkReader;

/**
 * Created by Avery on 10/22/17.
 */
@Autonomous (name = "JewelglyphAuto")
@Disabled
public class JewelglyphAuto extends LinearOpMode {

    ColorSensor16Bit snsColorB;
    ColorSensor16Bit snsColorT;
    ColorSensor16Bit snsColorF;
    //ColorSensor16Bit snsColorJ;
    ModernRoboticsI2cColorSensor snsColorJewel;
    ModernRoboticsI2cGyro snsGyro;
    DcMotor mtrFL;
    DcMotor mtrFR;
    DcMotor mtrBL;
    DcMotor mtrBR;
    Encoder encFL;
    Servo svoJewel;
    Servo svoColor;
    Servo svoStone;
    DcMotor mtrCollector;
    Servo svoGlyph1;
    Servo svoGlyph2;
    Servo svoCollect;
    Servo orientationSVO;
    Servo dispensingSVO;

    @Override
    public void runOpMode() {

        // do the vuforia stuff
        RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.UNKNOWN;
        VuMarkReader vuMarkReader = new VuMarkReader(hardwareMap.appContext);
        vuMarkReader.start();

        mtrCollector = hardwareMap.dcMotor.get("mtrCollector");
        svoGlyph1 = hardwareMap.servo.get("svoGlyph1");
        svoGlyph2 = hardwareMap.servo.get("svoGlyph2");
        svoJewel = hardwareMap.servo.get("svoJewel");
        svoCollect = hardwareMap.servo.get("svoCollect");
        orientationSVO = hardwareMap.servo.get("orientationSVO");
        dispensingSVO = hardwareMap.servo.get("dispensingSVO");
        svoCollect = hardwareMap.servo.get("svoCollect");
        svoCollect.setPosition(0.5);

        mtrBL = hardwareMap.dcMotor.get("mtrBL");
        mtrBR = hardwareMap.dcMotor.get("mtrBR");
        mtrFL = hardwareMap.dcMotor.get("mtrFL");
        mtrFR = hardwareMap.dcMotor.get("mtrFR");
        svoJewel = hardwareMap.servo.get("svoJewel");
        svoColor = hardwareMap.servo.get("svoColor");
        svoStone = hardwareMap.servo.get("svoStone");
        svoStone.scaleRange(0, 1);
        svoStone.setPosition(0.17647);
        svoJewel.setPosition(0.8);

        encFL = new Encoder(mtrFL);

        mtrFL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrFR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mtrBR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        snsGyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "snsGyro");
        snsColorB = new ColorSensor16Bit(hardwareMap, "snsColorB", 0x3c);
        snsColorF = new ColorSensor16Bit(hardwareMap, "snsColorF", 0x3a);
        snsColorT = new ColorSensor16Bit(hardwareMap, "snsColorT", 0x3e);
        //snsColorJ = new ColorSensor16Bit(hardwareMap, "snsColorJ", 0x38);
        snsColorJewel = (ModernRoboticsI2cColorSensor) hardwareMap.get(ColorSensor.class, "snsColorJ");
        snsColorJewel.setI2cAddress(I2cAddr.create8bit(0x38));
        snsColorJewel.enableLed(true);
        dispensingSVO.setPosition(0.08);
        orientationSVO.setPosition(0.3529);
        svoGlyph1.setPosition(0.5);
        svoGlyph2.setPosition(0.5);
        snsGyro.calibrate();


        /*
            so, you see, the problem with waitForStart(); is that we need to continuously poll the
            vumark reader in a loop to get the vumark, right? the problem with that, however, is that
            waitForStart(); basically pauses the program until the play button is pressed. so we do a workaround:
        */



        while (!isStarted() && !isStopRequested()) { // basically loops until the play button is pressed

            // only record a seen vumark if its actually useful:
            RelicRecoveryVuMark v = vuMarkReader.getVuMark();
            if (v != RelicRecoveryVuMark.UNKNOWN)
                vuMark = v;

            // give us some details on the status of the vumark and the gyro sensor
            telemetry.addData("Gyro calibrating: ", snsGyro.isCalibrating());
            telemetry.addData("VuMark", vuMark.toString());
            telemetry.update();
        }

        // shut down the vumark reader as the opmode starts
        vuMarkReader.stop();

        if (isStopRequested()) return;

        // this is where waitForStart usually is

        /*
         of course, once you have the vumark, how do you use it? well, it's an enum, so you can compare it to stuff.
         Here's a simple example using if statements:

         if (vuMark == RelicRecoveryVuMark.LEFT) {
            // left code
         } else if (vuMark == RelicRecoveryVuMark.RIGHT) {
            // right code
         } else {
            // center code, also code run if we have no idea what the vumark is
         }


         here's an alternate example using the switch statement:

         switch (vuMark) {
            case RelicRecoveryVuMark.LEFT:
                // left code
                break;
            case RelicRecoveryVuMark.RIGHT:
                // right code
                break;
            case RelicRecoveryVuMark.CENTER:
            default:
                // center code, also code run if we have no idea what the vumark is
                break;
          }

          I'm just going to assume you know vaguely what you're doing and leave you to use the above code as you see fit
        */

        //Here is where we will insert an if statement that checks the state of the physical switches on the robot
        //there will be two switches that determine the position and the color of the robot's alliance.
        //for now I will just put in a simple boolean set to a set value to run specific code until we get the physical switch
        snsGyro.resetZAxisIntegrator();
        dispensingSVO.setPosition(0.08);
        svoGlyph1.setPosition(0);
        svoGlyph2.setPosition(1);

//        int[] Values16B = snsColorB.ReadColor16();
//        int blue = Values16B[2];
//        int[] Values16T = snsColorB.ReadColor16();
//        int redT = Values16T[0];
        SenseJewel();
        dispensingSVO.setPosition(0.08);
        sleep(1000);
            encFL.resetEncoder();
        while(encFL.getEncValue() < (1120*1.30) && opModeIsActive()) {
            DrivePower(-0.3, 0.3, -0.3, 0.3);
        }
            DrivePower(0, 0, 0, 0);
            svoColor.setPosition(0.3529);
            sleep(500);
            snsGyro.resetZAxisIntegrator();
            int currentHeading = snsGyro.getIntegratedZValue();
            int targetHeading = 80;

            /*double base_power = 1;
            double p = 0.002;
            while (currentHeading < targetHeading) {
                double error = targetHeading - currentHeading;
                double power = Range.clip(base_power * error * p, -1, 1);
                DrivePower(power, power, power, power);
                currentHeading = snsGyro.getIntegratedZValue();
            }*/
            while (currentHeading < targetHeading){
                DrivePower(0.2, 0.2, 0.2, 0.2);
                currentHeading = snsGyro.getIntegratedZValue();
            }
            DrivePower(0, 0, 0, 0);
            sleep(500);
            encFL.resetEncoder();
        while (encFL.getEncValue() > (-400)){
            DrivePower(0.1, -0.1, 0.1, -0.1);
        }
            DrivePower(0, 0, 0, 0);
            svoColor.setPosition(0.452);
        PlaceGlyph(vuMark);

    }
    public void DrivePower(double fr, double fl, double br, double bl) {
        mtrBL.setPower(bl);
        mtrBR.setPower(br);
        mtrFL.setPower(fl);
        mtrFR.setPower(fr);
    }
    public void SenseJewel() {
        svoJewel.setPosition(0);
        sleep(3000);
        int blueJ = snsColorJewel.readUnsignedShort(ModernRoboticsI2cColorSensor.Register.NORMALIZED_BLUE_READING);


        if (blueJ > 2000) {
            encFL.resetEncoder();
            while (encFL.getEncValue() > -90) {
                DrivePower(-0.1, -0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            svoJewel.setPosition(1);
            encFL.resetEncoder();
            while (encFL.getEncValue() < 90) {
                DrivePower(0.1, 0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);

        } else {
            encFL.resetEncoder();
            while (encFL.getEncValue() < 90) {
                DrivePower(0.1, 0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            svoJewel.setPosition(1);
            encFL.resetEncoder();
            while (encFL.getEncValue() > -90) {
                DrivePower(-0.1, -0.1, -0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);

        }
    }
    public void PlaceGlyph(RelicRecoveryVuMark vuMark){
        if (vuMark == RelicRecoveryVuMark.LEFT) {
            // left code
            telemetry.addLine("LEFT SIDE BOYS");
            telemetry.update();
            encFL.resetEncoder();
            while (encFL.getEncValue() < (70)){
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);



        } else if (vuMark == RelicRecoveryVuMark.RIGHT) {
            // right code

            telemetry.addLine("RIGHT SIDE BOYS");
            telemetry.update();
            encFL.resetEncoder();
            while (encFL.getEncValue() < (400)){
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            orientationSVO.setPosition(0.1098);
            dispensingSVO.setPosition(0.2);
            sleep(300);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-400)){
                DrivePower(-0.1, -0.1, 0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);




            //Placing glyph code
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-120)){
                DrivePower(0.1, -0.1, 0.1, -0.1);
            }
            DrivePower(0, 0, 0, 0);
            svoGlyph1.setPosition(0.46);
            svoGlyph2.setPosition(0.86);
            sleep(1000);
            encFL.resetEncoder();
            while (encFL.getEncValue() > (-230)){
                DrivePower(0.4, -0.4, 0.4, -0.4);
            }
            DrivePower(0, 0, 0, 0);

            encFL.resetEncoder();
            while (encFL.getEncValue() < 300){
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);
            dispensingSVO.setPosition(0.2);
            svoCollect.setPosition(1);
            sleep(5000);
            svoCollect.setPosition(0.5);
            dispensingSVO.setPosition(0.08);



        } else if (vuMark == RelicRecoveryVuMark.CENTER) {
            // center code
            telemetry.addLine("CENTER BOYS");
            telemetry.update();
            encFL.resetEncoder();
            while (encFL.getEncValue() < (70)){
                DrivePower(-0.1, 0.1, -0.1, 0.1);
            }
            DrivePower(0, 0, 0, 0);

            int[] Values16B = snsColorB.ReadColor16();
            int blue = Values16B[2];

        } else {
            telemetry.addLine("Vuforia error, could not detect image");
            telemetry.update();
            //still run center code
        }
    }
}
